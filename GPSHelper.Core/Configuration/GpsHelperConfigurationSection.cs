﻿using System.Configuration;
using System.Linq;
using GPSHelper.Core.Configuration.Elements;

namespace GPSHelper.Core.Configuration
{
    public class GpsHelperConfigurationSection : ConfigurationSection
    {
        #region Members

        public ProviderSettings GetElementByKey(string key)
        {
            return
                Modules.Cast<ProviderSettings>().FirstOrDefault(element => element.Name.ToUpper().Equals(key.ToUpper()));
        }

        #endregion

        #region Properties

        [ConfigurationProperty("locationManager", IsRequired = true)]
        public LocationManagerElement LocationManager
        {
            get { return (LocationManagerElement) base["locationManager"]; }
            set { base["locationManager"] = value; }
        }

        [ConfigurationProperty("inputService", IsRequired = true)]
        public InputServiceElement InputService
        {
            get { return (InputServiceElement) base["inputService"]; }
            set { base["inputService"] = value; }
        }

        [ConfigurationProperty("locationProvider", IsRequired = true)]
        public LocationProviderElement LocationProvider
        {
            get { return (LocationProviderElement) base["locationProvider"]; }
            set { base["locationProvider"] = value; }
        }

        [ConfigurationProperty("inputProvider", IsRequired = true)]
        public InputProviderElement inputProvider
        {
            get { return (InputProviderElement) base["inputProvider"]; }
            set { base["inputProvider"] = value; }
        }

        [ConfigurationProperty("extensions", IsRequired = true)]
        public ProviderSettingsCollection Modules => (ProviderSettingsCollection) base["extensions"];

        #endregion
    }
}