// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="HotKeyElement.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Configuration;

namespace GPSHelper.Core.Configuration.Elements
{
    /// <summary>
    ///     Represents HotKey ConfigurationElement.
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class HotKeyElement : ConfigurationElement
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the description attribute.
        /// </summary>
        /// <value>The description.</value>
        [ConfigurationProperty("Description", IsRequired = true, IsKey = true)]
        public string Description
        {
            get { return (string) base["Description"]; }
            set { base["Description"] = value; }
        }

        /// <summary>
        ///     Gets or sets the value attribute.
        /// </summary>
        /// <value>The value.</value>
        [ConfigurationProperty("value", IsRequired = true, IsKey = true)]
        public string Value
        {
            get { return (string) base["value"]; }
            set { base["value"] = value; }
        }

        /// <summary>
        ///     Gets or sets the action attribute.
        /// </summary>
        /// <value>The action.</value>
        [ConfigurationProperty("action", IsRequired = true)]
        public string Action
        {
            get { return (string) base["action"]; }
            set { base["action"] = value; }
        }

        #endregion
    }
}