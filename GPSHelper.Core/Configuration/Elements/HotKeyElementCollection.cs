﻿// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="HotKeyElementCollection.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Configuration;

namespace GPSHelper.Core.Configuration.Elements
{
    /// <summary>
    ///     Class HotKeyElementCollection.
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElementCollection" />
    public class HotKeyElementCollection : ConfigurationElementCollection
    {
        #region Members

        /// <summary>
        ///     When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement" /> with
        ///     custom implementation.
        /// </summary>
        /// <returns>A newly created <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new HotKeyElement();
        }

        /// <summary>
        ///     Gets the element key for a specified configuration element when overridden in a derived class. This implementation
        ///     uses the value attribute as the key.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to return the key for.</param>
        /// <returns>
        ///     An <see cref="T:System.Object" /> that acts as the key for the specified
        ///     <see cref="T:System.Configuration.ConfigurationElement" />.
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((HotKeyElement) element).Value;
        }

        /// <summary>
        ///     Gets the element by key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>HotKeyElement.</returns>
        public HotKeyElement GetElementByKey(string key)
        {
            foreach (HotKeyElement element in this)
            {
                if (element.Value.ToUpper().Equals(key.ToUpper()))
                    return element;
            }

            return null;
        }

        #endregion
    }
}