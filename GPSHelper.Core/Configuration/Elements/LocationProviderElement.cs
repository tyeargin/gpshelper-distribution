// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="LocationProviderElement.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Configuration;

namespace GPSHelper.Core.Configuration.Elements
{
    /// <summary>
    ///     Represents the LocationProvider configuration element.
    /// </summary>
    /// <seealso cref="ProviderTypeElement" />
    public class LocationProviderElement : ProviderTypeElement
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the movement threshold attribute. This attribute specifies the sensitivity of the location watcher.
        /// </summary>
        /// <value>The movement threshold.</value>
        [ConfigurationProperty("movementThreshold", IsRequired = true)]
        public string MovementThreshold
        {
            get { return (string) base["movementThreshold"]; }
            set { base["movementThreshold"] = value; }
        }

        /// <summary>
        ///     Gets or sets the useHighPositionalAccuracy attribute.
        /// </summary>
        /// <value>The use high positional accuracy.</value>
        [ConfigurationProperty("useHighPositionalAccuracy", IsRequired = true)]
        public string UseHighPositionalAccuracy
        {
            get { return (string) base["useHighPositionalAccuracy"]; }
            set { base["useHighPositionalAccuracy"] = value; }
        }

        /// <summary>
        ///     Gets or sets the onlineUrl attributes. Default value is http://maps.google.com/maps?z=20&t=m&q=loc:{0}+{1}.
        /// </summary>
        /// <value>The online URL.</value>
        [ConfigurationProperty("onlineUrl", IsRequired = true,
            DefaultValue = "http://maps.google.com/maps?z=20&t=m&q=loc:{0}+{1}")]
        public string OnlineUrl
        {
            get { return (string) base["onlineUrl"]; }
            set { base["onlineUrl"] = value; }
        }

        #endregion
    }
}