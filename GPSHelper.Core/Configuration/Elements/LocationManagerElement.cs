// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="LocationManagerElement.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Configuration;
using GPSHelper.Common.Contracts;

namespace GPSHelper.Core.Configuration.Elements
{
    /// <summary>
    ///     Represents LocationManager configuration element.
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class LocationManagerElement : ConfigurationElement
    {
        #region Members

        /// <summary>
        ///     Gets the default provider configured for the location manager. This is for convenience.
        /// </summary>
        /// <returns>ProviderSettings.</returns>
        internal ProviderSettings GetDefaultProvider()
        {
            return Providers[DefaultProvider];
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the providers configured the the location manager.
        /// </summary>
        /// <value>The providers.</value>
        [ConfigurationProperty("providers", IsRequired = true)]
        public ProviderSettingsCollection Providers => (ProviderSettingsCollection) base["providers"];

        /// <summary>
        ///     Gets or sets the default provider configured for the location manager.
        /// </summary>
        /// <value>The default provider.</value>
        [StringValidator(MinLength = 1), ConfigurationProperty("defaultProvider", DefaultValue = "LocationProvider")]
        public string DefaultProvider
        {
            get { return Convert.ToString(base["defaultProvider"]); }
            set { base["defaultProvider"] = value; }
        }

        /// <summary>
        ///     Gets or sets the location provider.
        /// </summary>
        /// <value>The location provider.</value>
        internal ILocationProvider LocationProvider { get; set; }

        #endregion
    }
}