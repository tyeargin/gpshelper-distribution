// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="InputProviderElement.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Core.Configuration.Elements
{
    /// <summary>
    ///     Represents InputProvider configuration element.
    /// </summary>
    /// <seealso cref="ProviderTypeElement" />
    public class InputProviderElement : ProviderTypeElement
    {
    }
}