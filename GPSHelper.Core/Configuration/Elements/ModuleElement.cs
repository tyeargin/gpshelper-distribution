// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-19-2016
// ***********************************************************************
// <copyright file="ModuleElement.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Configuration;

namespace GPSHelper.Core.Configuration.Elements
{
    /// <summary>
    ///     Represents Module configuration elements.
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class ModuleElement : ConfigurationElement
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the name attribute.
        /// </summary>
        /// <value>The name.</value>
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string) base["name"]; }
            set { base["name"] = value; }
        }

        /// <summary>
        ///     Gets or sets the type attribute.
        /// </summary>
        /// <value>The type.</value>
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return (string) base["type"]; }
            set { base["type"] = value; }
        }

        #endregion
    }
}