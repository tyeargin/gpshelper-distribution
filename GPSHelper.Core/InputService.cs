﻿// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="InputService.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Windows.Forms;
using GPSHelper.Common.Actions;
using GPSHelper.Common.Contracts;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Events;
using GPSHelper.Common.Modules;
using GPSHelper.Core.Configuration;
using GPSHelper.Core.Configuration.Elements;
using GPSHelper.Core.Contracts;

namespace GPSHelper.Core
{
    /// <summary>
    ///     Input Service provides functionality for sending keyboard input to Windows. Also detects hotkey presses.
    /// </summary>
    /// <seealso cref="IInputService" />
    public class InputService : IInputService
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputService" /> class.
        /// </summary>
        /// <param name="configurationFactory">The configuration factory.</param>
        public InputService(IConfigurationFactory configurationFactory)
        {
            ActionDictionary = new ActionDictionary();

            InputProvider = configurationFactory.Configuration.InputService.InputProvider;

            //setup hotkeys
            HotKeys = configurationFactory.Configuration.InputService.HotKeys;
            InputProvider.HotKeys = configurationFactory.HotKeys;

            Configuration = configurationFactory.Configuration;

            //subscribe to events
            Events = configurationFactory.GpsHelperEvents;
            Events.InputProviderAvailable?.Invoke(new ShareInputProviderEventArgs(InputProvider));
        }

        #region Properties

        /// <summary>
        ///     Gets or sets the application events that the service and subscribe to.
        /// </summary>
        /// <value>The events.</value>
        public GpsHelperEvents Events { get; set; }

        /// <summary>
        ///     Gets or sets the current configuration.
        /// </summary>
        /// <value>The configuration.</value>
        public GpsHelperConfigurationSection Configuration { get; set; }

        /// <summary>
        ///     Gets or sets the hot keys.
        /// </summary>
        /// <value>The hot keys.</value>
        public HotKeyElementCollection HotKeys { get; set; }

        /// <summary>
        ///     Gets or sets the input provider.
        /// </summary>
        /// <value>The input provider.</value>
        public IInputProvider InputProvider { get; set; }

        #endregion

        #region Members

        /// <summary>
        ///     Sends the text.
        /// </summary>
        /// <param name="input">The input.</param>
        void IInputService.SendText(string input)
        {
            InputProvider.SendText(input);
        }

        /// <summary>
        ///     Handles the keyboard input.
        /// </summary>
        /// <param name="vkCode">The virtual key code that was detected.</param>
        /// <param name="modifierKeys">
        ///     The modifier keys that were pressed. These include CTRL and SHIFT. ALT is handled separately
        ///     because it is a system key.
        /// </param>
        /// <param name="altModifier">if set to <c>true</c>ALT modifier is present.</param>
        /// <returns><c>true</c> if key or key combination is an application "special key", <c>false</c> otherwise.</returns>
        InputResult IInputService.HandleInput(Keys vkCode, Keys modifierKeys, bool altModifier)
        {
            if (HotKeysEnabled)
            {
                return InputProvider.HandleInput(vkCode, modifierKeys, altModifier);
            }
            return new InputResult(null, null, true);
        }

        /// <summary>
        ///     Registers a hot key and assigns an action.
        /// </summary>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="action">The action.</param>
        /// <exception cref="System.ApplicationException"></exception>
        public void RegisterHotKey(string actionName, Action action)
        {
            if (ActionDictionary == null)
                ActionDictionary = new ActionDictionary();

            if (!ActionDictionary.ContainsKey(actionName))
            {
                ActionDictionary.Add(actionName, action);
            }
            else
            {
                throw new ApplicationException(string.Format("The action {0} is already registered.", actionName));
            }
        }

        /// <summary>
        ///     Toggles the hot keys on and off.
        /// </summary>
        public void ToggleHotKeys()
        {
            HotKeysEnabled = !HotKeysEnabled;
        }

        /// <summary>
        ///     Gets or sets a value indicating whether [hot keys enabled].
        /// </summary>
        /// <value><c>true</c> if [hot keys enabled]; otherwise, <c>false</c>.</value>
        public bool HotKeysEnabled { get; set; } = true;

        /// <summary>
        ///     Gets or sets the action dictionary.
        /// </summary>
        /// <value>The action dictionary.</value>
        public ActionDictionary ActionDictionary { get; set; }

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        public void Dispose()
        {
            if (InputProvider != null)
            {
                InputProvider.Dispose();
            }
        }

        /// <summary>
        ///     Sends the key.
        /// </summary>
        /// <param name="key">The key.</param>
        public void SendKey(VirtualKeyCode key)
        {
            InputProvider.SendKey(key);
        }

        #endregion
    }
}