﻿// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-20-2016
// ***********************************************************************
// <copyright file="IInputService.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Windows.Forms;
using GPSHelper.Common.Actions;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Modules;

namespace GPSHelper.Core.Contracts
{
    /// <summary>
    ///     Interface IInputService
    /// </summary>
    public interface IInputService
    {
        /// <summary>
        ///     Gets or sets the action dictionary.
        /// </summary>
        /// <value>The action dictionary.</value>
        ActionDictionary ActionDictionary { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [hot keys enabled].
        /// </summary>
        /// <value><c>true</c> if [hot keys enabled]; otherwise, <c>false</c>.</value>
        bool HotKeysEnabled { get; set; }

        /// <summary>
        ///     Sends the text.
        /// </summary>
        /// <param name="input">The input.</param>
        void SendText(string input);

        /// <summary>
        ///     Sends the key.
        /// </summary>
        /// <param name="tab">The tab.</param>
        void SendKey(VirtualKeyCode tab);

        /// <summary>
        ///     Handles the input.
        /// </summary>
        /// <param name="vkCode">The vk code.</param>
        /// <param name="modifierKeys">The modifier keys.</param>
        /// <param name="altModifier">if set to <c>true</c> [alt modifier].</param>
        /// <returns>InputResult.</returns>
        InputResult HandleInput(Keys vkCode, Keys modifierKeys, bool altModifier);

        /// <summary>
        ///     Registers the hot key.
        /// </summary>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="action">The action.</param>
        void RegisterHotKey(string actionName, Action action);

        /// <summary>
        ///     Toggles the hot keys on and off.
        /// </summary>
        void ToggleHotKeys();

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        void Dispose();
    }
}