using GPSHelper.Core.Configuration;

namespace GPSHelper.Core.Contracts
{
    /// <summary>
    ///     Interface IConfigurableService
    /// </summary>
    public interface IConfigurableService
    {
        /// <summary>
        ///     Gets or sets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        GpsHelperConfigurationSection Configuration { get; set; }
    }
}