using System.Collections.Generic;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Events;
using GPSHelper.Common.Modules;
using GPSHelper.Core.Configuration;

namespace GPSHelper.Core.Contracts
{
    /// <summary>
    ///     Interface IConfigurationFactory
    /// </summary>
    public interface IConfigurationFactory
    {
        /// <summary>
        ///     Gets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        GpsHelperConfigurationSection Configuration { get; }

        /// <summary>
        ///     Gets the hot keys.
        /// </summary>
        /// <value>The hot keys.</value>
        List<HotKey> HotKeys { get; }

        /// <summary>
        ///     Gets the GPS helper events.
        /// </summary>
        /// <returns>GpsHelperEvents.</returns>
        GpsHelperEvents GpsHelperEvents { get; }

        void LoadModules();
    }
}