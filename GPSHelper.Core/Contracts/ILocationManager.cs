﻿// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-19-2016
// ***********************************************************************
// <copyright file="ILocationManager.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using GPSHelper.Common.Entities;

namespace GPSHelper.Core.Contracts
{
    /// <summary>
    ///     Interface ILocationManager
    /// </summary>
    public interface ILocationManager
    {
        /// <summary>
        ///     Gets the state.
        /// </summary>
        /// <value>The state.</value>
        State State { get; }

        /// <summary>
        ///     Gets the permission.
        /// </summary>
        /// <value>The permission.</value>
        AccessPermission Permission { get; }

        /// <summary>
        ///     Gets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        double Longitude { get; }

        /// <summary>
        ///     Gets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        double Latitude { get; }

        /// <summary>
        ///     Gets or sets a value indicating whether [enable abbreviated coordinates].
        /// </summary>
        /// <value><c>true</c> if [enable abbreviated coordinates]; otherwise, <c>false</c>.</value>
        bool EnableAbbreviatedCoordinates { get; set; }

        /// <summary>
        ///     Gets or sets the location information.
        /// </summary>
        /// <value>The location information.</value>
        LocationInfo LocationInfo { get; set; }

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        void Dispose();

        /// <summary>
        ///     Stops this instance.
        /// </summary>
        void Stop();

        /// <summary>
        ///     Starts the specified do not request permission.
        /// </summary>
        /// <param name="doNotRequestPermission">if set to <c>true</c> [do not request permission].</param>
        void Start(bool doNotRequestPermission);

        /// <summary>
        ///     Views the location online.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        void ViewLocationOnline(object sender, EventArgs args);

        /// <summary>
        ///     Gets the location information.
        /// </summary>
        /// <returns>LocationInfo.</returns>
        LocationInfo GetLocationInfo();

        /// <summary>
        ///     Updates the coordinates.
        /// </summary>
        void UpdateCoordinates();
    }
}