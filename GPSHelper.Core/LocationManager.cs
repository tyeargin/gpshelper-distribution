﻿// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-22-2016
// ***********************************************************************
// <copyright file="LocationManager.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using GPSHelper.Common.Contracts;
using GPSHelper.Common.Entities;
using GPSHelper.Common.ErrorHandling;
using GPSHelper.Common.Events;
using GPSHelper.Common.Logging;
using GPSHelper.Common.Modules;
using GPSHelper.Core.Configuration;
using GPSHelper.Core.Contracts;
using GPSHelper.Core.Properties;

namespace GPSHelper.Core
{
    /// <summary>
    ///     The LocationManager providers functionality for getting the current location.
    /// </summary>
    /// <seealso cref="ILocationManager" />
    /// <seealso cref="IConfigurableService" />
    public class LocationManager : ILocationManager, IConfigurableService
    {
        /// <summary>
        ///     When true indicates that lower precision coordinates can be output.
        /// </summary>
        private bool _enableAbbreviatedCoordinates = true;

        /// <summary>
        ///     Initializes a new instance of the <see cref="LocationManager" /> class.
        /// </summary>
        /// <param name="configurationFactory">The configuration factory.</param>
        public LocationManager(IConfigurationFactory configurationFactory)
        {
            Logger = new Logger(LoggerImplementation.Log4Net);
            Configuration = configurationFactory.Configuration;

            //configuration LocationProvider
            LocationProvider = configurationFactory.Configuration.LocationManager.LocationProvider;
            LocationProvider.Logger = Logger;

            //subscribe to events
            Events = configurationFactory.GpsHelperEvents;
            Events.LocationProviderAvailable?.Invoke(new ShareLocationProviderEventArgs(LocationProvider));
            Events.HotKeyActionsRegistrationAvailable += RegisterHotKeyActions;
            LocationProvider.CoordinatesUpdated += ProviderCoordinatesUpdated;

            EnableAbbreviatedCoordinates = true;
        }

        #region Properties

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        /// <value>The logger.</value>
        public ILogger Logger { get; set; }

        /// <summary>
        ///     Gets or sets the events.
        /// </summary>
        /// <value>The events.</value>
        public GpsHelperEvents Events { get; set; }

        /// <summary>
        ///     Gets or sets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        public GpsHelperConfigurationSection Configuration { get; set; }

        /// <summary>
        ///     Gets or sets the location provider.
        /// </summary>
        /// <value>The location provider.</value>
        public ILocationProvider LocationProvider { get; set; }

        /// <summary>
        ///     Gets the state.
        /// </summary>
        /// <value>The state.</value>
        public State State
        {
            get { return LocationProvider.State; }
        }

        /// <summary>
        ///     Gets the permission.
        /// </summary>
        /// <value>The permission.</value>
        public AccessPermission Permission
        {
            get { return LocationProvider.AccessPermission; }
        }

        /// <summary>
        ///     Gets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude
        {
            get
            {
                UpdateCoordinates();

                return LocationInfo.Longitude;
            }
        }

        /// <summary>
        ///     Gets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude
        {
            get
            {
                UpdateCoordinates();

                return LocationInfo.Latitude;
            }
        }

        /// <summary>
        ///     Updates the coordinates using the current geo position data as the source.
        /// </summary>
        /// <exception cref="LocationStatusException">
        /// </exception>
        public void UpdateCoordinates()
        {
            if (State == State.Unknown)
            {
                if (Permission == AccessPermission.Denied ||
                    Permission == AccessPermission.Unknown)
                {
                    Logger.Warning(Resources.LocationPlatform_DeniedState);
                }
                else
                {
                    Logger.Warning(Resources.LocationPlatform_DeniedOrUnknownState);
                }
            }

            if (State == State.Disabled)
            {
                Logger.Warning(
                    Resources.LocationPlatform_DisabledState);
            }

            if (State == State.Initializing)
            {
                Logger.Warning(
                    Resources.LocationPlatform_InitializingState);
            }

            LocationInfo = new LocationInfo(LocationProvider.Latitude,
                LocationProvider.Longitude);

            var args = new CoordinatesUpdatedEventArgs(EnableAbbreviatedCoordinates, LocationInfo);

            Events.CoordinatesUpdated?.Invoke(args);

            LocationInfo = args.Coordinates;
        }

        /// <summary>
        ///     Providers the coordinates updated.
        /// </summary>
        /// <param name="e">The <see cref="CoordinatesUpdatedEventArgs" /> instance containing the event data.</param>
        private void ProviderCoordinatesUpdated(CoordinatesUpdatedEventArgs e)
        {
            UpdateCoordinates();
        }

        /// <summary>
        ///     Gets or sets the location information.
        /// </summary>
        /// <value>The location information.</value>
        public LocationInfo LocationInfo { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [enable abbreviated coordinates].
        /// </summary>
        /// <value><c>true</c> if [enable abbreviated coordinates]; otherwise, <c>false</c>.</value>
        public bool EnableAbbreviatedCoordinates
        {
            get { return _enableAbbreviatedCoordinates; }
            set
            {
                _enableAbbreviatedCoordinates = value;
                LocationProvider.AllowLowerPrecision = _enableAbbreviatedCoordinates;
            }
        }

        /// <summary>
        ///     Views the location online.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        public void ViewLocationOnline(object sender, EventArgs args)
        {
            LocationProvider.ViewLocationOnline(sender, args);
        }

        /// <summary>
        ///     Gets the location information.
        /// </summary>
        /// <returns>LocationInfo.</returns>
        public LocationInfo GetLocationInfo()
        {
            UpdateCoordinates();

            return LocationInfo;
        }

        #endregion

        #region Members

        /// <summary>
        ///     Registers the hot key actions.
        /// </summary>
        /// <param name="e">The <see cref="RegisterHotKeyActionEventArgs" /> instance containing the event data.</param>
        private void RegisterHotKeyActions(RegisterHotKeyActionEventArgs e)
        {
            e.ActionDictionary.Add("ShowLocationOnline", () => { ViewLocationOnline(null, null); });
        }

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        public void Dispose()
        {
            if (LocationProvider != null)
            {
                LocationProvider.Stop();
                LocationProvider.Dispose();
            }
        }

        /// <summary>
        ///     Stops this instance.
        /// </summary>
        public void Stop()
        {
            LocationProvider.Stop();
        }

        /// <summary>
        ///     Starts the specified do not request permission.
        /// </summary>
        /// <param name="doNotRequestPermission">if set to <c>true</c> [do not request permission].</param>
        public void Start(bool doNotRequestPermission = false)
        {
            LocationProvider.Start(doNotRequestPermission);
        }

        #endregion
    }
}