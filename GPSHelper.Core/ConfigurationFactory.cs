﻿// ***********************************************************************
// Assembly         : GPSHelper.Core
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-21-2016
// ***********************************************************************
// <copyright file="ConfigurationFactory.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GPSHelper.Common.Contracts;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Events;
using GPSHelper.Common.Logging;
using GPSHelper.Common.Modules;
using GPSHelper.Common.RegularExpressions;
using GPSHelper.Core.Configuration;
using GPSHelper.Core.Configuration.Elements;
using GPSHelper.Core.Contracts;
using GPSHelper.Core.Properties;

namespace GPSHelper.Core
{
    /// <summary>
    ///     The ConfigurationFactory class provides functionality for reading the configuration and instantiating configured
    ///     services, providers, and modules.
    /// </summary>
    /// <seealso cref="IConfigurationFactory" />
    public class ConfigurationFactory : IConfigurationFactory
    {
        /// <summary>
        ///     Stores the configuration read in from the application configuration file.
        /// </summary>
        private readonly GpsHelperConfigurationSection _config;

        /// <summary>
        ///     Backing property for GPSHelperEvents.
        /// </summary>
        private readonly GpsHelperEvents _gpsHelperEvents;

        /// <summary>
        ///     Backing property for HotKeys.
        /// </summary>
        private readonly List<HotKey> _hotKeys;

        /// <summary>
        ///     Backing property for InputProvider.
        /// </summary>
        private readonly IInputProvider _inputProvider;

        /// <summary>
        ///     Backing property for LocationProvider.
        /// </summary>
        private readonly ILocationProvider _locationProvider;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConfigurationFactory" /> class.
        /// </summary>
        /// <exception cref="ConfigurationErrorsException"></exception>
        /// <exception cref="System.ApplicationException">
        /// </exception>
        public ConfigurationFactory()
        {
            try
            {
                Logger = new Logger(LoggerImplementation.Log4Net);
                _config = ConfigurationManager.GetSection("core") as GpsHelperConfigurationSection;
            }
            catch (ConfigurationErrorsException ce)
            {
                var error = string.Format("Invalid configuration. {0} at line {1} of the configuration file {2}",
                    ce.BareMessage, ce.Line, ce.Filename);

                var match = Regex.Match(ce.Message, Pattern.MissingConfigurationAttribute);

                if (match.Success && ce.Message == match.Value)
                {
                    error = string.Format("Missing required attribute {0} on line {1} of the configuration file.",
                        match.Groups[1], ce.Line, Path.GetFileName(ce.Filename));
                }

                match = Regex.Match(ce.Message, Pattern.UnrecognizedConfigurationAttribute);

                if (match.Success && ce.Message == match.Value)
                {
                    error = string.Format("Unrecognized attribute {0} on line {1} of the configuration file.",
                        match.Groups[1], ce.Line, Path.GetFileName(ce.Filename));
                }

                throw new ConfigurationErrorsException(error);
            }

            if (_config != null)
            {
                if (_config.LocationManager?.GetDefaultProvider() != null)
                {
                    ProviderSettings provider = null;

                    try
                    {
                        if (_config.LocationManager.GetDefaultProvider().Type != null)
                        {
                            provider = _config.LocationManager.GetDefaultProvider();
                            var useHighPositionalAccuracy = provider.Parameters["useHighPositionalAccuracy"];
                            var movementThreshold = provider.Parameters["movementThreshold"];

                            _locationProvider =
                                Activator.CreateInstance(Type.GetType(provider.Type),
                                    useHighPositionalAccuracy != null && bool.Parse(useHighPositionalAccuracy),
                                    movementThreshold != null ? int.Parse(movementThreshold) : 25) as ILocationProvider;
                            _config.LocationManager.LocationProvider = _locationProvider;
                        }
                    }
                    catch (ArgumentException e)
                    {
                        var match = Regex.Match(e.Message, Pattern.TypeArgumentNull);

                        if (match.Success && e.Message == match.Value)
                        {
                            throw new ApplicationException(
                                string.Format(
                                    "An error occurred while attempted to create an instance of {0}. Type may not have been implemented.",
                                    provider.Type));
                        }
                    }
                }

                if (_config.InputService != null)
                {
                    if (_config.InputService.GetDefaultProvider() != null)
                    {
                        if (_config.InputService.GetDefaultProvider().Type != null)
                        {
                            var provider = _config.InputService.GetDefaultProvider();

                            try
                            {
                                _inputProvider = Activator.CreateInstance(Type.GetType(provider.Type)) as IInputProvider;
                                _config.InputService.InputProvider = _inputProvider;
                            }
                            catch (ArgumentException e)
                            {
                                var match = Regex.Match(e.Message, Pattern.TypeArgumentNull);

                                if (match.Success && e.Message == match.Value)
                                {
                                    throw new ApplicationException(
                                        string.Format(
                                            "An error occurred while attempted to create an instance of {0}. Type may not have been implemented.",
                                            provider.Type));
                                }
                            }
                        }
                    }

                    if (_inputProvider != null)
                    {
                        if (_config.InputService.HotKeys != null)
                        {
                            _hotKeys = new List<HotKey>();

                            foreach (HotKeyElement hotKey in _config.InputService.HotKeys)
                            {
                                var value = hotKey.Value.ToUpper();

                                var match = Regex.Match(value, Pattern.HotKeyPattern);

                                var parts = new List<string>();

                                foreach (var group in match.Groups)
                                {
                                    if (!group.ToString().Contains("+") && !string.IsNullOrEmpty(group.ToString()))
                                    {
                                        parts.Add(group.ToString());
                                    }
                                }

                                var control = parts.Any(x => x.Equals(Keys.Control.ToString().ToUpper()));
                                var shift = parts.Any(x => x.Equals(Keys.Shift.ToString().ToUpper()));
                                var alt = parts.Any(x => x.Equals(Keys.Alt.ToString().ToUpper()));

                                var key = Keys.None;

                                if (parts.Count == 1)
                                {
                                    if (alt)
                                        key = Keys.Alt;

                                    if (control)
                                        key = Keys.Control;

                                    if (shift)
                                        key = Keys.Shift;

                                    value = _inputProvider.ConvertInput(key, control ? Keys.Control : Keys.None,
                                        shift ? Keys.Shift : Keys.None, alt ? Keys.Alt : Keys.None);
                                }
                                else
                                {
                                    var keyPart = parts.FirstOrDefault(
                                        p =>
                                            !p.Equals(Keys.Control.ToString().ToUpper()) &&
                                            !p.Equals(Keys.Alt.ToString().ToUpper()) &&
                                            !p.Equals(Keys.Shift.ToString().ToUpper()));

                                    if (keyPart != null)
                                    {
                                        value = _inputProvider.ConvertInput(keyPart, control ? Keys.Control : Keys.None,
                                            shift ? Keys.Shift : Keys.None, alt ? Keys.Alt : Keys.None);
                                    }
                                    else
                                    {
                                        value = _inputProvider.ConvertInput(key, control ? Keys.Control : Keys.None,
                                            shift ? Keys.Shift : Keys.None, alt ? Keys.Alt : Keys.None);
                                    }
                                }

                                ((IConfigurationFactory) this).HotKeys.Add(new HotKey(value,
                                    hotKey.Action, hotKey.Description));
                            }
                        }

                        if (_config.LocationProvider.OnlineUrl != null)
                        {
                            _locationProvider.Url = _config.LocationProvider.OnlineUrl;
                        }
                    }
                }

                _gpsHelperEvents = new GpsHelperEvents();

                LoadModules();
            }
        }

        public void LoadModules()
        {
            if (_config.Modules != null)
            {
                foreach (ProviderSettings moduleElement in _config.Modules)
                {
                    if (moduleElement.Type != null)
                    {
                        try
                        {
                            var module =
                                Activator.CreateInstance(Type.GetType(moduleElement.Type)) as IGpsHelperModule;
                            module.Initialize(_gpsHelperEvents, moduleElement.Parameters);
                        }
                        catch (ArgumentException e)
                        {
                            var match = Regex.Match(e.Message, Pattern.TypeArgumentNull);

                            if (match.Success && e.Message == match.Value)
                            {
                                Logger.Info(string.Format(Resources.ExtensionLoadFailure, moduleElement.Name));
                            }
                        }
                    }
                }
            }
        }

        #region Members

        /// <summary>
        ///     Gets the GPS helper events.
        /// </summary>
        /// <returns>GpsHelperEvents.</returns>
        GpsHelperEvents IConfigurationFactory.GpsHelperEvents => _gpsHelperEvents;

        #endregion

        #region Properties

        /// <summary>
        ///     Backing property for Logger.
        /// </summary>
        /// <value>The logger.</value>
        private ILogger Logger { get; }

        /// <summary>
        ///     Gets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        GpsHelperConfigurationSection IConfigurationFactory.Configuration => _config;

        /// <summary>
        ///     Gets the hot keys.
        /// </summary>
        /// <value>The hot keys.</value>
        List<HotKey> IConfigurationFactory.HotKeys
        {
            get { return _hotKeys; }
        }

        #endregion
    }
}