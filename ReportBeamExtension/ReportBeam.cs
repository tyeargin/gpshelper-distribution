﻿// ***********************************************************************
// Assembly         : ReportBeamExtension
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-25-2016
// ***********************************************************************
// <copyright file="ReportBeam.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Specialized;
using System.Globalization;
using GPSHelper.Common.Contracts;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Events;
using GPSHelper.Common.Modules;

namespace ReportBeamExtension
{
    /// <summary>
    ///     ReportBeam extension extends the application with functionality for formatting the coordinates
    ///     output to fit the requirements of ReportBeam and also register a hotkey for inputting the coordinates into
    ///     ReportBeam.
    /// </summary>
    /// <seealso cref="GPSHelper.Common.Modules.IGpsHelperModule" />
    public class ReportBeam : IGpsHelperModule
    {
        /// <summary>
        ///     Gets or sets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public NameValueCollection Parameters { get; set; }

        /// <summary>
        ///     Gets or sets the location provider.
        /// </summary>
        /// <value>The location provider.</value>
        public ILocationProvider LocationProvider { get; set; }

        /// <summary>
        ///     Gets or sets the input provider.
        /// </summary>
        /// <value>The input provider.</value>
        public IInputProvider InputProvider { get; set; }

        /// <summary>
        ///     Initializes an instance of the implementation.
        /// </summary>
        /// <param name="events">The applications events that the module can subscribe to.</param>
        /// <param name="parameters">The configuration parameters for the module.</param>
        public void Initialize(GpsHelperEvents events, NameValueCollection parameters)
        {
            events.CoordinatesUpdated += CoordinatesUpdated;
            events.InputProviderAvailable += ShareInputProvider;
            events.LocationProviderAvailable += ShareLocationProvider;
            events.HotKeyActionsRegistrationAvailable += RegisterHotKeyActions;

            Parameters = parameters;
        }

        /// <summary>
        ///     Registers the hot key actions.
        /// </summary>
        /// <param name="registerHotKeyActionEventArgs">
        ///     The <see cref="RegisterHotKeyActionEventArgs" /> instance containing the
        ///     event data.
        /// </param>
        private void RegisterHotKeyActions(RegisterHotKeyActionEventArgs registerHotKeyActionEventArgs)
        {
            if (InputProvider != null && LocationProvider != null)
            {
                registerHotKeyActionEventArgs.ActionDictionary?.Add("OutputReportBeamCoordinates", () =>
                {
                    if (LocationProvider.AllowLowerPrecision)
                    {
                        InputProvider.SendText(
                            Math.Round(LocationProvider.Latitude, 5).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        InputProvider.SendText(LocationProvider.Latitude.ToString(CultureInfo.InvariantCulture));
                    }

                    InputProvider.SendKey(VirtualKeyCode.TAB);

                    if (LocationProvider.AllowLowerPrecision)
                    {
                        InputProvider.SendText(
                            Math.Round(LocationProvider.Longitude, 5).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        InputProvider.SendText(LocationProvider.Longitude.ToString(CultureInfo.InvariantCulture));
                    }
                });
            }
        }

        /// <summary>
        ///     Shares the location provider.
        /// </summary>
        /// <param name="shareLocationProviderEventArgs">
        ///     The <see cref="ShareLocationProviderEventArgs" /> instance containing the
        ///     event data.
        /// </param>
        private void ShareLocationProvider(ShareLocationProviderEventArgs shareLocationProviderEventArgs)
        {
            LocationProvider = shareLocationProviderEventArgs.LocationProvider;
        }

        /// <summary>
        ///     Shares the input provider.
        /// </summary>
        /// <param name="shareInputProviderEventArgs">
        ///     The <see cref="ShareInputProviderEventArgs" /> instance containing the event
        ///     data.
        /// </param>
        private void ShareInputProvider(ShareInputProviderEventArgs shareInputProviderEventArgs)
        {
            InputProvider = shareInputProviderEventArgs.InputProvider;
        }

        /// <summary>
        ///     Coordinateses the updated.
        /// </summary>
        /// <param name="e">The <see cref="CoordinatesUpdatedEventArgs" /> instance containing the event data.</param>
        private void CoordinatesUpdated(CoordinatesUpdatedEventArgs e)
        {
            if (e.AllowLowerPrecision)
            {
                e.Coordinates.Longitude = Math.Round(e.Coordinates.Longitude, 5);
                e.Coordinates.Latitude = Math.Round(e.Coordinates.Latitude, 5);
            }
        }
    }
}