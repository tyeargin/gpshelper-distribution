﻿// ***********************************************************************
// Assembly         : GPSHelper.Providers
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="InputProvider.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using GPSHelper.Common.Contracts;
using GPSHelper.Common.Entities;
using GPSHelper.Common.RegularExpressions;

namespace GPSHelper.Providers
{
    /// <summary>
    ///     InputProvider provides functionality for simulating keyboard input.
    /// </summary>
    /// <seealso cref="GPSHelper.Common.Contracts.IInputProvider" />
    public class InputProvider : IInputProvider
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputProvider" /> class.
        /// </summary>
        public InputProvider()
        {
            ((IInputProvider) this).Initialize();
        }

        /// <summary>
        ///     Initializes this instance of IInputProvider implementation.
        /// </summary>
        void IInputProvider.Initialize()
        {
        }

        /// <summary>
        ///     Sends the text to destination.
        /// </summary>
        /// <param name="text">The text.</param>
        void IInputProvider.SendText(string text)
        {
            KeyboardSimulator.SendText(text);
        }

        /// <summary>
        ///     Sends the key to destination.
        /// </summary>
        /// <param name="key">The key.</param>
        void IInputProvider.SendKey(VirtualKeyCode key)
        {
            KeyboardSimulator.SendKey(key);
        }

        /// <summary>
        ///     Converts the input to a valid "special key" If input cannot be converted then null is returned.
        /// </summary>
        /// <param name="key">The virtual key code that was detected.</param>
        /// <param name="control">The control modifier key.</param>
        /// <param name="shift">The shift modifier key.</param>
        /// <param name="alt">The alt modifier key.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="ApplicationException"></exception>
        /// <remarks>
        ///     Will always convert input for format such as CTRL+SHIFT+ALT+{key}. If no modifier is present then output will
        ///     simply be {key}
        /// </remarks>
        string IInputProvider.ConvertInput(Keys key, Keys control, Keys shift, Keys alt)
        {
            try
            {
                var keyCode =
                    ((!control.ToString().Equals(Keys.None.ToString()) ? control + "+" : "") +
                     (!shift.ToString().Equals(Keys.None.ToString()) ? shift + "+" : "") +
                     (!alt.ToString().Equals(Keys.None.ToString()) ? alt + "+" : "") +
                     (!key.ToString().Equals(Keys.None.ToString()) ? key.ToString() : "")).ToUpper();

                if (!string.IsNullOrEmpty(keyCode))
                    keyCode =
                        keyCode.Replace(Keys.OemPeriod.ToString().ToUpper(), ".")
                            .Replace(Keys.Oemcomma.ToString().ToUpper(), ",");

                if (keyCode.Contains("PACKET"))
                    return null;

                var patternMatch = Regex.Match(keyCode, Pattern.HotKeyPattern).Value;
                if (keyCode == patternMatch)
                {
                    return keyCode;
                }

                return null;
            }
            catch (Exception e)
            {
                throw;
                //throw new ApplicationException(Resources.InputTranslationError + e.Message); //TODO
            }
        }

        /// <summary>
        ///     Determines if the input is a valid HotKey or should be ignored.
        /// </summary>
        /// <param name="vkCode">The vk code.</param>
        /// <param name="modifierKeys">The modifier keys.</param>
        /// <param name="altModifier">if set to <c>true</c> Alt was pressed in combination with the key.</param>
        /// <returns>InputResult.</returns>
        InputResult IInputProvider.HandleInput(Keys vkCode, Keys modifierKeys, bool altModifier)
        {
            try
            {
                var ignoreKeyList = new List<Keys>
                {
                    Keys.LShiftKey,
                    Keys.LControlKey,
                    Keys.RShiftKey,
                    Keys.RControlKey,
                    Keys.None
                };

                var shift = Control.ModifierKeys & Keys.Shift;
                var control = Control.ModifierKeys & Keys.Control;
                var alt = Control.ModifierKeys & Keys.Alt;

                //handle special cases
                if (vkCode == Keys.LMenu || vkCode == Keys.RMenu)
                {
                    vkCode = Keys.Alt;
                }

                if (vkCode == Keys.LShiftKey || vkCode == Keys.RShiftKey)
                {
                    vkCode = Keys.Shift;
                }

                if (vkCode == Keys.LControlKey || vkCode == Keys.RControlKey)
                {
                    vkCode = Keys.Control;
                }

                if (altModifier)
                {
                    alt = Keys.Alt;
                }

                if (!ignoreKeyList.Contains(vkCode))
                {
                    var keyCode = ((IInputProvider) this).ConvertInput(vkCode, control, shift, alt);
                    Console.WriteLine(keyCode);
                    if (keyCode != null)
                    {
                        var hotkey = ((IInputProvider) this).HotKeys.FirstOrDefault(k => k.Value.Equals(keyCode));

                        if (!string.IsNullOrEmpty(hotkey?.Value))
                        {
                            return new InputResult(hotkey.Value, hotkey.Action, false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw;
                //MessageBox.Show(Resources.InputHandlingError + "Exception:" + e.Message); TODO
            }

            return new InputResult(null, null, true);
        }

        /// <summary>
        ///     Converts the input.
        /// </summary>
        /// <param name="keyPart">The key part.</param>
        /// <param name="control">The control.</param>
        /// <param name="shift">The shift.</param>
        /// <param name="alt">The alt.</param>
        /// <returns>System.String.</returns>
        string IInputProvider.ConvertInput(string keyPart, Keys control, Keys shift, Keys alt)
        {
            var key = Keys.None;

            if (keyPart.Equals("."))
            {
                key = Keys.OemPeriod;
            }

            if (keyPart.Equals(","))
            {
                key = Keys.Oemcomma;
            }

            if (keyPart.Equals("`"))
            {
                key = Keys.Oemtilde;
            }

            return ((IInputProvider) this).ConvertInput(key, control, shift, alt);
        }

        /// <summary>
        ///     Gets or sets the hot keys defined by the application.
        /// </summary>
        /// <value>The hot keys.</value>
        List<HotKey> IInputProvider.HotKeys { get; set; }

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        void IInputProvider.Dispose()
        {
            Dispose();
        }

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Dispose()
        {
            //Not supported
        }
    }
}