﻿// ***********************************************************************
// Assembly         : GPSHelper.Providers
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-25-2016
// ***********************************************************************
// <copyright file="LocationProvider.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Device.Location;
using System.Diagnostics;
using GPSHelper.Common.Contracts;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Events;
using GPSHelper.Common.Logging;
using GPSHelper.Common.Modules;

namespace GPSHelper.Providers
{
    /// <summary>
    ///     This is the default location provider class. It extends <c>System.Device.Location.GeoCoordinateWatcher</c>.
    ///     Providers current GPS Coordinates.
    /// </summary>
    /// <seealso cref="System.Device.Location.GeoCoordinateWatcher" />
    /// <seealso cref="GPSHelper.Common.Contracts.ILocationProvider" />
    public class LocationProvider : GeoCoordinateWatcher, ILocationProvider
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LocationProvider" /> class.
        /// </summary>
        /// <param name="useHighAccuracy">if set to <c>true</c> demand high accuracy from the location sensor.</param>
        /// <param name="movementThreshold">
        ///     The movement threshold determines how frequently a location update is requested based
        ///     on distance changed in meters. So if value is 1 then location updates are polled every 1 meter of recognized change
        ///     in distance.
        /// </param>
        public LocationProvider(bool useHighAccuracy, int movementThreshold)
            : base(useHighAccuracy ? GeoPositionAccuracy.High : GeoPositionAccuracy.Default)
        {
            MovementThreshold = movementThreshold;

            Initialize();
        }

        #region Members

        /// <summary>
        ///     Initializes this instance of location provider.
        /// </summary>
        /// <exception cref="System.ApplicationException">An error occurred while initializing location integration.</exception>
        public void Initialize()
        {
            try
            {
                PositionChanged +=
                    (sender, args) =>
                    {
                        LocationInfo = new LocationInfo(args.Position.Location.Latitude,
                            args.Position.Location.Longitude);

                        CoordinatesUpdated?.Invoke(new CoordinatesUpdatedEventArgs(AllowLowerPrecision, LocationInfo));
                    };

                //set the initial coordinates
                LocationInfo = new LocationInfo(Position.Location.Latitude,
                    Position.Location.Longitude);

                Start(false);
            }
            catch (Exception e)
            {
                Logger.Error("An error occurred while initializing location integration.");
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Event handler for handling request to view location online.
        /// </summary>
        /// <value>The view location online.</value>
        EventHandler ILocationProvider.ViewLocationOnline
        {
            get
            {
                return (sender, args) =>
                {
                    var url = string.Format(((ILocationProvider) this).Url, Latitude,
                        Longitude);
                    Process.Start(url);
                };
            }
        }

        /// <summary>
        ///     Gets or sets the url to use for viewing the location online.
        /// </summary>
        /// <value>The URL.</value>
        string ILocationProvider.Url { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating whether lower-precision coordinates are output.
        /// </summary>
        /// <value><c>true</c> if [allow lower precision]; otherwise, <c>false</c>.</value>
        public bool AllowLowerPrecision { get; set; } = true;

        /// <summary>
        ///     Delegated event that is fired when coordinates are updated.
        /// </summary>
        /// <value>The coordinates updated.</value>
        public GpsHelperDelegate<CoordinatesUpdatedEventArgs> CoordinatesUpdated { get; set; }

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        /// <value>The logger.</value>
        public ILogger Logger { get; set; }

        /// <summary>
        ///     Gets or sets the current coordinates.
        /// </summary>
        /// <value>The coordinates.</value>
        public LocationInfo LocationInfo { get; set; }

        /// <summary>
        ///     Gets the latitude of the current position.
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude => Position.Location.Latitude;

        /// <summary>
        ///     Gets the longitude of the current position.
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude => Position.Location.Longitude;

        /// <summary>
        ///     Gets the current state of the GeoPositioning sensor.
        /// </summary>
        /// <value>The state.</value>
        public State State
        {
            get
            {
                if (Status == GeoPositionStatus.Disabled)
                    return State.Disabled;

                if (Status == GeoPositionStatus.Ready)
                    return State.Ready;

                if (Status == GeoPositionStatus.Initializing)
                    return State.Initializing;

                if (Status == GeoPositionStatus.NoData)
                    return State.Unknown;

                return State.Unknown;
            }
        }

        /// <summary>
        ///     Gets the access permission of the GeoPosition source.
        /// </summary>
        /// <value>The access permission.</value>
        public AccessPermission AccessPermission
        {
            get
            {
                if (Permission == GeoPositionPermission.Granted)
                    return AccessPermission.Allowed;

                if (Permission == GeoPositionPermission.Denied)
                    return AccessPermission.Denied;

                return AccessPermission.Unknown;
            }
        }

        #endregion
    }
}