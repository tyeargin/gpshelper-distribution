﻿// ***********************************************************************
// Assembly         : GPSHelper.Providers
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="KeyboardSimulator.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using WindowsInput;
using VirtualKeyCode = GPSHelper.Common.Entities.VirtualKeyCode;

namespace GPSHelper.Providers
{
    /// <summary>
    ///     KeyboardSimulator exposes key functionality of InputSimulator.
    /// </summary>
    public class KeyboardSimulator
    {
        /// <summary>
        ///     Sends virtual key input for a string of text.
        /// </summary>
        /// <param name="text">The text to send.</param>
        /// <exception cref="System.ApplicationException">An error occurred while sending text input:  + e.Message</exception>
        /// <exception cref="ApplicationException">An error occurred while sending text input:  + e.Message</exception>
        internal static void SendText(string text)
        {
            try
            {
                InputSimulator.SimulateTextEntry(text);
            }
            catch (Exception e)
            {
                throw new ApplicationException("An error occurred while sending text input: " + e.Message);
            }
        }


        /// <summary>
        ///     Sends virtual keyboard input for a single key.
        /// </summary>
        /// <param name="key">The virtual key code to simulate.</param>
        /// <exception cref="System.ApplicationException">An error occurred while sending key input:  + e.Message</exception>
        /// <exception cref="ApplicationException">An error occurred while sending key input:  + e.Message</exception>
        internal static void SendKey(VirtualKeyCode key)
        {
            var input =
                (WindowsInput.VirtualKeyCode)
                    Enum.Parse(typeof (WindowsInput.VirtualKeyCode), Enum.GetName(typeof (VirtualKeyCode), key));

            try
            {
                InputSimulator.SimulateKeyPress(input);
            }
            catch (Exception e)
            {
                throw new ApplicationException("An error occurred while sending key input: " + e.Message);
            }
        }
    }
}