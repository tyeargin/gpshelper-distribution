﻿using System;
using System.Collections.Specialized;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using GPSHelper;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Modules;
using Microsoft.Win32;

namespace DesktopOverlayExtension
{
    public class Overlay : IGpsHelperModule
    {
        public Form Container { get; private set; }

        public void Initialize(GpsHelperEvents events, NameValueCollection parameters)
        {
            events.FormLoaded += FormLoaded;
            events.CoordinatesUpdated += CoordinatesUpdated;
            events.RegisterHotKeyActions += RegisterHotKeyActions;

            SystemEvents.DisplaySettingsChanged += new EventHandler(SystemEvents_DisplaySettingsChanged);

            this.Parameters = parameters;
        }

        private void SystemEvents_DisplaySettingsChanged(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CoordinatesUpdated(CoordinatesUpdatedEventArgs e)
        {
            if (LongitudeText != null && LatitudeText != null)
            {
                var longitudeCoordinates = Math.Round(e.Coordinates.Longitude, 8).ToString(CultureInfo.InvariantCulture);
                var latitudeCoordinates = Math.Round(e.Coordinates.Latitude, 8).ToString(CultureInfo.InvariantCulture);

                LatitudeText.Text = latitudeCoordinates;
                LongitudeText.Text = longitudeCoordinates;
            }
        }

        private void RegisterHotKeyActions(RegisterHotKeyActionEventArgs registerHotKeyActionEventArgs)
        {
            registerHotKeyActionEventArgs.ActionDictionary?.Add("ToggleOverlay", () =>
            {
                this.Container.Visible = this.Container.Visible != true;
            });
        }

        public NameValueCollection Parameters { get; set; }

        private void FormLoaded(FormLoadedEventArgs e)
        {
            this.Container = e.Container;

            this.GpsTitleLabel = e.GPSInfoTitle;
            this.LatitudeLabel = e.LatitudeLabel;
            this.LongitudeLabel = e.LongitudeLabel;
            this.LatitudeText = e.LatitudeText;
            this.LongitudeText = e.LongitudeText;

            PlaceLowerRight();
            this.Container.LocationChanged += ContainerOnLocationChanged;
        }

        private void PlaceLowerRight()
        {
            //Determine "rightmost" screen
            Screen rightmost = Screen.AllScreens[0];
            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen.WorkingArea.Right > rightmost.WorkingArea.Right)
                    rightmost = screen;
            }

            this.Container.Height = 100;
            Taskbar taskbar = new Taskbar();

            int offsetLeft = 0;
            int offsetTop = 0;

            offsetLeft = Parameters.Get("offset_left") == null ? offsetLeft : int.Parse(Parameters.Get("offset_left"));
            offsetTop = Parameters.Get("offset_top") == null ? offsetTop : int.Parse(Parameters.Get("offset_top"));

            this.Container.Left = rightmost.WorkingArea.Right - this.Container.Width - offsetLeft;
            this.Container.Top = rightmost.WorkingArea.Bottom - this.Container.Height - offsetTop;

            OriginalLeft = rightmost.WorkingArea.Right - this.Container.Width - offsetLeft;
            OriginalTop = rightmost.WorkingArea.Bottom - this.Container.Height - offsetTop;

            Color textColor = Color.FromKnownColor(KnownColor.OrangeRed);

            var color = Parameters.Get("color");

            if (color != null)
                textColor = Color.FromName(color) != Color.Empty ? Color.FromName(color) : textColor;           

            GpsTitleLabel.ForeColor = textColor;
            LatitudeLabel.ForeColor = textColor;
            LongitudeLabel.ForeColor = textColor;
            LatitudeText.ForeColor = textColor;
            LongitudeText.ForeColor = textColor;

            LatitudeText.Text = "Unknown";
            LongitudeText.Text = "Unknown";
        }

        public Control GpsTitleLabel { get; set; }
        public Control LatitudeLabel { get; set; }
        public Control LongitudeLabel { get; set; }
        public Control LatitudeText { get; set; }
        public Control LongitudeText { get; set; }

        public int OriginalLeft { get; set; }

        public int OriginalTop { get; set; }
        public int LengthOffset { get; private set; }

        private void ContainerOnLocationChanged(object sender, EventArgs eventArgs)
        {
            this.Container.Left = OriginalLeft;
            this.Container.Top = OriginalTop;
        }
    }
}