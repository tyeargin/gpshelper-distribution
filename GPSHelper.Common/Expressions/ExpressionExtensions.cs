﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-23-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="ExpressionExtensions.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Linq.Expressions;
using System.Reflection;

namespace GPSHelper.Common.Expressions
{
    /// <summary>
    ///     Expression extensions.
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        ///     Returns the name of the specified property of the specified type.
        /// </summary>
        /// <typeparam name="T">The type the property is a member of.</typeparam>
        /// <param name="property">The property.</param>
        /// <returns>The property name.</returns>
        public static string GetMember<T>(this Expression<Func<T, object>> property)
        {
            LambdaExpression lambda = property;
            MemberExpression memberExpression;

            var body = lambda.Body as UnaryExpression;

            if (body != null)
            {
                var unaryExpression = body;
                memberExpression = (MemberExpression) unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression) lambda.Body;
            }

            return ((PropertyInfo) memberExpression.Member).Name;
        }
    }
}