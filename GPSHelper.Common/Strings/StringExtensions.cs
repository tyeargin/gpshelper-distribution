﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-23-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="StringExtensions.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;

namespace GPSHelper.Common.Strings
{
    /// <summary>
    ///     Class StringExtensions.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        ///     Removes value provided from the original string.
        /// </summary>
        /// <param name="value">The original string.</param>
        /// <param name="remove">The value to be removed.</param>
        /// <returns>String.</returns>
        public static string Drop(this string value, string remove)
        {
            var output = (value ?? string.Empty).Replace(remove, string.Empty);
            return output;
        }

        /// <summary>
        ///     Removes value provided from the original string.
        /// </summary>
        /// <param name="value">The original string.</param>
        /// <param name="remove">The value to be removed.</param>
        /// <returns>String.</returns>
        /// <example>
        ///     <code>
        /// var originalString = "Test123".
        /// var alteredString = originalString.Drop("123")
        /// //alteredString = "Test"
        /// </code>
        /// </example>
        public static string Drop(this object value, string remove)
        {
            var output = (value as string ?? string.Empty).Replace(remove, string.Empty);
            return output;
        }

        /// <summary>
        ///     Formats a string by injecting the provided strings.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="strings">The strings.</param>
        /// <returns>System.String.</returns>
        /// <example>
        ///     var result = "This is a {0} {1}.".Format(new object[] { "simple", "example" }).
        ///     //result = "This is a simple example."
        /// </example>
        public static string Format(this string template, object[] strings)
        {
            return string.Format(CultureInfo.InvariantCulture, template, strings);
        }

        /// <summary>
        ///     Formats a string by injecting the provided string.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="input">The input.</param>
        /// <returns>System.String.</returns>
        /// <example>
        ///     <code>
        /// var result = "This is a {0}.".FormatSingle("string").
        /// //result = "This is a string."
        /// </code>
        /// </example>
        public static string FormatSingle(this string template, string input)
        {
            return string.Format(template, input);
        }

        /// <summary>
        ///     Formats a string by making the first letter of each word capitalized.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>System.String.</returns>
        /// <example>
        ///     var titleCaseString = "originalString".ToTitleCase();
        ///     //titleCaseString = "OriginalString"
        ///     var singleWordString = "original".ToTitleCase();
        ///     //singleWordString = "Original"
        /// </example>
        public static string ToTitleCase(this string template)
        {
            return Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(template.ToLower());
        }

        /// <summary>
        ///     Gets the string with spaces. In most cases property names are simply converted to string
        ///     with spaces between each word in property name.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>System.String.</returns>
        /// <example>
        ///     <code>
        /// var originalString = "FirstSecondThird".GetStringWithSpaces();
        /// //originalString = "First Second Third"
        /// </code>
        /// </example>
        public static string GetStringWithSpaces(this string input)
        {
            return Regex.Replace(
                input,
                "(?<!^)" +
                "(" +
                "  [A-Z][a-z] |" +
                "  (?<=[a-z])[A-Z] |" +
                "  (?<![A-Z])[A-Z]$" +
                ")",
                " $1",
                RegexOptions.IgnorePatternWhitespace);
        }

        /// <summary>
        ///     Compares string equality in a case sensitive fashion.
        /// </summary>
        /// <param name="value">The string value to compare for equality.</param>
        /// <param name="comparison">The string value to compare against.</param>
        /// <returns>String.</returns>
        /// <remarks>Trims the input strings.</remarks>
        public static bool IsEqualTo(this string value, string comparison)
        {
            return value.Trim().Equals(comparison.Trim(), StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        ///     Checks if string is null or whitespace.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>Bool.</returns>
        public static bool IsNullOrWhiteSpace(this string value)
        {
            var output = string.IsNullOrWhiteSpace(value ?? string.Empty);
            return output;
        }

        /// <summary>
        ///     Checks if value is of provided object type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The string value.</param>
        /// <returns>Bool.</returns>
        public static bool IsType<T>(this string value)
        {
            return value.Is<T>();
        }

        /// <summary>
        ///     Converts string to provided object type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The string value.</param>
        /// <returns>Bool.</returns>
        public static object To<T>(this string value)
        {
            return value.As<T>();
        }

        /// <summary>
        ///     Removes dashes ("-") from the given object value represented as a string and returns an empty string ("")
        ///     when the instance type could not be represented as a string.
        ///     <para>
        ///         Note: This will return the type name of given instance if the runtime type of the given instance is not a
        ///         string!.
        ///     </para>
        ///     .
        /// </summary>
        /// <param name="value">The object instance to undash when represented as its string value.</param>
        /// <returns>String.</returns>
        public static string UnDash(this object value)
        {
            var output = (value as string ?? string.Empty).UnDash();
            return output;
        }

        /// <summary>
        ///     Removes dashes the specified from the given string value.
        /// </summary>
        /// <param name="value">The string value that optionally contains dashes.</param>
        /// <returns>String.</returns>
        /// <example>
        ///     <code>
        /// var test = "This-is-a-test".UnDash;
        /// //test = "Thisisatest"
        /// </code>
        /// </example>
        public static string UnDash(this string value)
        {
            var output = Drop(value, "-");
            return output;
        }
    }
}