﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-23-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="StringConversionExtensions.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.ComponentModel;
using System.Globalization;

namespace GPSHelper.Common.Strings
{
    /// <summary>
    ///     Provides utility methods for converting string values to other data types.
    /// </summary>
    public static class StringConversionExtensions
    {
        /// <summary>
        ///     Converts a string to a strongly typed value of the specified data type.
        /// </summary>
        /// <typeparam name="TValue">The data type to convert to.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public static TValue As<TValue>(this string value)
        {
            return value.As(default(TValue));
        }

        /// <summary>
        ///     Converts a string to the specified data type and specifies a default value.
        /// </summary>
        /// <typeparam name="TValue">The data type to convert to.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <param name="defaultValue">The value to return if <paramref name="value" /> is null.</param>
        /// <returns>The converted value.</returns>
        public static TValue As<TValue>(this string value, TValue defaultValue)
        {
            TValue tValue;
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof (TValue));
                if (!converter.CanConvertFrom(typeof (string)))
                {
                    converter = TypeDescriptor.GetConverter(typeof (string));
                    if (!converter.CanConvertTo(typeof (TValue)))
                    {
                        return defaultValue;
                    }
                    tValue = (TValue) converter.ConvertTo(value, typeof (TValue));
                }
                else
                {
                    tValue = (TValue) converter.ConvertFrom(value);
                }
            }
            catch
            {
                return defaultValue;
            }
            return tValue;
        }

        /// <summary>
        ///     Converts a string to a Boolean (true/false) value.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public static bool AsBool(this string value)
        {
            return value.AsBool(false);
        }

        /// <summary>
        ///     Converts a string to a Boolean (true/false) value and specifies a default value.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="defaultValue">The value to return if <paramref name="value" /> is null or is an invalid value.</param>
        /// <returns>The converted value.</returns>
        public static bool AsBool(this string value, bool defaultValue)
        {
            bool flag;
            if (!bool.TryParse(value, out flag))
            {
                return defaultValue;
            }
            return flag;
        }

        /// <summary>
        ///     Converts a string to a <see cref="T:System.DateTime" /> value.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public static DateTime AsDateTime(this string value)
        {
            return value.AsDateTime(new DateTime());
        }

        /// <summary>
        ///     Converts a string to a <see cref="T:System.DateTime" /> value and specifies a default value.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="defaultValue">
        ///     The value to return if <paramref name="value" /> is null or is an invalid value. The default
        ///     is the minimum time value on the system.
        /// </param>
        /// <returns>The converted value.</returns>
        public static DateTime AsDateTime(this string value, DateTime defaultValue)
        {
            DateTime dateTime;
            if (!DateTime.TryParse(value, out dateTime))
            {
                return defaultValue;
            }
            return dateTime;
        }

        /// <summary>
        ///     Converts a string to a <see cref="T:System.Decimal" /> number.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public static decimal AsDecimal(this string value)
        {
            return value.As<decimal>();
        }

        /// <summary>
        ///     Converts a string to a <see cref="T:System.Decimal" /> number and specifies a default value.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="defaultValue">The value to return if <paramref name="value" /> is null or invalid.</param>
        /// <returns>The converted value.</returns>
        public static decimal AsDecimal(this string value, decimal defaultValue)
        {
            return value.As(defaultValue);
        }

        /// <summary>
        ///     Converts a string to a <see cref="T:System.Single" /> number.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public static float AsFloat(this string value)
        {
            return value.AsFloat(0f);
        }

        /// <summary>
        ///     Converts a string to a <see cref="T:System.Single" /> number and specifies a default value.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="defaultValue">The value to return if <paramref name="value" /> is null.</param>
        /// <returns>The converted value.</returns>
        public static float AsFloat(this string value, float defaultValue)
        {
            float single;
            if (!float.TryParse(value, out single))
            {
                return defaultValue;
            }
            return single;
        }

        /// <summary>
        ///     Converts a string to an integer.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public static int AsInt(this string value)
        {
            return value.AsInt(0);
        }

        /// <summary>
        ///     Converts a string to an integer and specifies a default value.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="defaultValue">The value to return if <paramref name="value" /> is null or is an invalid value.</param>
        /// <returns>The converted value.</returns>
        public static int AsInt(this string value, int defaultValue)
        {
            int num;
            if (!int.TryParse(value, out num))
            {
                return defaultValue;
            }
            return num;
        }

        /// <summary>
        ///     Checks whether a string can be converted to the specified data type.
        /// </summary>
        /// <typeparam name="TValue">The data type to convert to.</typeparam>
        /// <param name="value">The value to test.</param>
        /// <returns>true if <paramref name="value" /> can be converted to the specified type; otherwise, false.</returns>
        public static bool Is<TValue>(this string value)
        {
            var converter = TypeDescriptor.GetConverter(typeof (TValue));

            try
            {
                if (value == null || converter.CanConvertFrom(null, value.GetType()))
                {
                    // ReSharper disable once AssignNullToNotNullAttribute
                    converter.ConvertFrom(null, CultureInfo.CurrentCulture, value);
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Checks whether a string can be converted to the Boolean (true/false) type.
        /// </summary>
        /// <param name="value">The string value to test.</param>
        /// <returns>true if <paramref name="value" /> can be converted to the specified type; otherwise, false.</returns>
        public static bool IsBool(this string value)
        {
            bool flag;
            return bool.TryParse(value, out flag);
        }

        /// <summary>
        ///     Checks whether a string can be converted to the <see cref="T:System.DateTime" /> type.
        /// </summary>
        /// <param name="value">The string value to test.</param>
        /// <returns>true if <paramref name="value" /> can be converted to the specified type; otherwise, false.</returns>
        public static bool IsDateTime(this string value)
        {
            DateTime dateTime;
            return DateTime.TryParse(value, out dateTime);
        }

        /// <summary>
        ///     Checks whether a string can be converted to the <see cref="T:System.Decimal" /> type.
        /// </summary>
        /// <param name="value">The string value to test.</param>
        /// <returns>true if <paramref name="value" /> can be converted to the specified type; otherwise, false.</returns>
        public static bool IsDecimal(this string value)
        {
            return value.Is<decimal>();
        }

        /// <summary>
        ///     Checks whether a string value is null or empty.
        /// </summary>
        /// <param name="value">The string value to test.</param>
        /// <returns>true if <paramref name="value" /> is null or is a zero-length string (""); otherwise, false.</returns>
        public static bool IsEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        ///     Checks whether a string can be converted to the <see cref="T:System.Single" /> type.
        /// </summary>
        /// <param name="value">The string value to test.</param>
        /// <returns>true if <paramref name="value" /> can be converted to the specified type; otherwise, false.</returns>
        public static bool IsFloat(this string value)
        {
            float single;
            return float.TryParse(value, out single);
        }

        /// <summary>
        ///     Checks whether a string can be converted to an integer.
        /// </summary>
        /// <param name="value">The string value to test.</param>
        /// <returns>true if <paramref name="value" /> can be converted to the specified type; otherwise, false.</returns>
        public static bool IsInt(this string value)
        {
            int num;
            return int.TryParse(value, out num);
        }
    }
}