// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="ActionDictionary.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;

namespace GPSHelper.Common.Actions
{
    /// <summary>
    ///     Stores a dictionary of action names and executable actions.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.Dictionary{System.String, System.Action}" />
    public class ActionDictionary : Dictionary<string, Action>
    {
        /// <summary>
        ///     Adds the specified key/action pair.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="action">The action.</param>
        public void Add(string key, Action action)
        {
            if (!ContainsKey(key))
            {
                base.Add(key, action);
            }
        }
    }
}