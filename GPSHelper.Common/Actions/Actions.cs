﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="Actions.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Common.Actions
{
    /// <summary>
    ///     Defines the actions that can be assigned a hot key.
    /// </summary>
    public class Actions
    {
        /// <summary>
        ///     Outputs the current latitude coordinate.
        /// </summary>
        public const string OutputLatitude = "OutputLatitude";

        /// <summary>
        ///     Outputs the current longitude coordinate.
        /// </summary>
        public const string OutputLongitude = "OutputLongitude";

        /// <summary>
        ///     Outputs the current coordinates.
        /// </summary>
        public const string OutputCoordinates = "OutputCoordinates";
    }
}