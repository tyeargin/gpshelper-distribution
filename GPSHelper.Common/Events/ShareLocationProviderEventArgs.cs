// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="ShareLocationProviderEventArgs.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.ComponentModel;
using GPSHelper.Common.Contracts;

namespace GPSHelper.Common.Events
{
    /// <summary>
    ///     Event arguments passed when LocationProvider is made available. Allows it to be shared with modules.
    /// </summary>
    /// <seealso cref="System.ComponentModel.CancelEventArgs" />
    public class ShareLocationProviderEventArgs : CancelEventArgs
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ShareLocationProviderEventArgs" /> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        public ShareLocationProviderEventArgs(ILocationProvider provider)
        {
            LocationProvider = provider;
        }

        /// <summary>
        ///     Gets or sets the location provider.
        /// </summary>
        /// <value>The location provider.</value>
        public ILocationProvider LocationProvider { get; set; }
    }
}