// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="RegisterHotKeyActionEventArgs.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.ComponentModel;
using GPSHelper.Common.Actions;

namespace GPSHelper.Common.Events
{
    /// <summary>
    ///     Event arguments that are passed to allow new hot keys to be created.
    /// </summary>
    /// <seealso cref="System.ComponentModel.CancelEventArgs" />
    public class RegisterHotKeyActionEventArgs : CancelEventArgs
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RegisterHotKeyActionEventArgs" /> class.
        /// </summary>
        /// <param name="actionDictionary">The action dictionary.</param>
        public RegisterHotKeyActionEventArgs(ActionDictionary actionDictionary)
        {
            ActionDictionary = actionDictionary;
        }

        /// <summary>
        ///     Gets or sets the action dictionary. The list of actions is passed as an event argument to allow subscribers to add
        ///     new actions.
        /// </summary>
        /// <value>The action dictionary.</value>
        public ActionDictionary ActionDictionary { get; set; }
    }
}