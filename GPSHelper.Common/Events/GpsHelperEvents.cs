﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="GpsHelperEvents.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using GPSHelper.Common.Modules;

namespace GPSHelper.Common.Events
{
    /// <summary>
    ///     GPSHelper application events.
    /// </summary>
    public class GpsHelperEvents
    {
        public GpsHelperDelegate<CoordinatesUpdatedEventArgs> CoordinatesUpdated { get; set; }
        public GpsHelperDelegate<RegisterHotKeyActionEventArgs> HotKeyActionsRegistrationAvailable { get; set; }
        public GpsHelperDelegate<ShareInputProviderEventArgs> InputProviderAvailable { get; set; }
        public GpsHelperDelegate<ShareLocationProviderEventArgs> LocationProviderAvailable { get; set; }
        public GpsHelperDelegate<ShareLoggerEventArgs> LoggerAvailable { get; set; }
        public GpsHelperDelegate<FormLoadedEventArgs> FormLoaded { get; set; }
    }
}