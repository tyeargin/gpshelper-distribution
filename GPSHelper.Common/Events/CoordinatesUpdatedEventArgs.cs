// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="CoordinatesUpdatedEventArgs.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.ComponentModel;
using GPSHelper.Common.Entities;

namespace GPSHelper.Common.Events
{
    /// <summary>
    ///     Events arguments that are passed when coordinates are updated.
    /// </summary>
    /// <seealso cref="System.ComponentModel.CancelEventArgs" />
    public class CoordinatesUpdatedEventArgs : CancelEventArgs
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CoordinatesUpdatedEventArgs" /> class.
        /// </summary>
        /// <param name="allowLowerPrecision">if set to <c>true</c> lower-precision coordinates are enabled.</param>
        /// <param name="locationInfo">The location information.</param>
        public CoordinatesUpdatedEventArgs(bool allowLowerPrecision, LocationInfo locationInfo)
        {
            AllowLowerPrecision = allowLowerPrecision;
            Coordinates = locationInfo;
        }

        /// <summary>
        ///     Gets or sets a value indicating whether lower-precision coordinates are enabled.
        /// </summary>
        /// <value><c>true</c> if [allow lower precision]; otherwise, <c>false</c>.</value>
        public bool AllowLowerPrecision { get; set; }

        /// <summary>
        ///     Gets or sets the coordinates.
        /// </summary>
        /// <value>The coordinates.</value>
        public LocationInfo Coordinates { get; set; }
    }
}