// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="FormLoadedEventArgs.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.ComponentModel;
using System.Windows.Forms;

namespace GPSHelper.Common.Events
{
    /// <summary>
    ///     Event arguments that are passed when the main container is loaded.
    /// </summary>
    /// <seealso cref="System.ComponentModel.CancelEventArgs" />
    public class FormLoadedEventArgs : CancelEventArgs
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FormLoadedEventArgs" /> class.
        /// </summary>
        /// <param name="form">The form.</param>
        public FormLoadedEventArgs(Form form)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FormLoadedEventArgs" /> class.
        /// </summary>
        /// <param name="form">The form.</param>
        /// <param name="gpsTitle">The GPS title.</param>
        /// <param name="latitudeLabel">The latitude label.</param>
        /// <param name="longitudeLabel">The longitude label.</param>
        /// <param name="latitudeText">The latitude text.</param>
        /// <param name="longitudeText">The longitude text.</param>
        public FormLoadedEventArgs(Form form, Label gpsTitle, Label latitudeLabel, Label longitudeLabel,
            Label latitudeText, Label longitudeText)
        {
            Container = form;
            GPSInfoTitle = gpsTitle;
            LatitudeLabel = latitudeLabel;
            LongitudeLabel = longitudeLabel;
            LatitudeText = latitudeText;
            LongitudeText = longitudeText;
        }

        /// <summary>
        ///     Gets or sets the default longitude coordinate text.
        /// </summary>
        /// <value>The longitude text.</value>
        public Label LongitudeText { get; set; }

        /// <summary>
        ///     Gets or sets the default latitude coordinate text.
        /// </summary>
        /// <value>The latitude text.</value>
        public Label LatitudeText { get; set; }

        /// <summary>
        ///     Gets or sets the longitude label control.
        /// </summary>
        /// <value>The longitude label.</value>
        public Label LongitudeLabel { get; set; }

        /// <summary>
        ///     Gets or sets the latitude label control.
        /// </summary>
        /// <value>The latitude label.</value>
        public Label LatitudeLabel { get; set; }

        /// <summary>
        ///     Gets or sets the GPS information title label control.
        /// </summary>
        /// <value>The GPS information title.</value>
        public Label GPSInfoTitle { get; set; }

        /// <summary>
        ///     Gets or sets the main container.
        /// </summary>
        /// <value>The container.</value>
        public Form Container { get; set; }
    }
}