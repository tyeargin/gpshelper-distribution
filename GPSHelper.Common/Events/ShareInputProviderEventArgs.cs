// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="ShareInputProviderEventArgs.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.ComponentModel;
using GPSHelper.Common.Contracts;

namespace GPSHelper.Common.Events
{
    /// <summary>
    ///     Event arguments passed when InputProvider is made available. Allows it to be shared with modules.
    /// </summary>
    /// <seealso cref="System.ComponentModel.CancelEventArgs" />
    public class ShareInputProviderEventArgs : CancelEventArgs
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ShareInputProviderEventArgs" /> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        public ShareInputProviderEventArgs(IInputProvider provider)
        {
            InputProvider = provider;
        }

        /// <summary>
        ///     Gets or sets the input provider.
        /// </summary>
        /// <value>The input provider.</value>
        public IInputProvider InputProvider { get; set; }
    }
}