// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="ShareLoggerEventArgs.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.ComponentModel;
using GPSHelper.Common.Contracts;
using GPSHelper.Common.Logging;

namespace GPSHelper.Common.Events
{
    /// <summary>
    ///     Event arguments passed when Logger is made available. Allows it to be shared with modules.
    /// </summary>
    /// <seealso cref="System.ComponentModel.CancelEventArgs" />
    public class ShareLoggerEventArgs : CancelEventArgs
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ShareLoggerEventArgs" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public ShareLoggerEventArgs(ILogger logger)
        {
            Logger = logger;
        }

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        /// <value>The logger.</value>
        public ILogger Logger { get; set; }

        /// <summary>
        ///     Gets or sets the location provider.
        /// </summary>
        /// <value>The location provider.</value>
        public ILocationProvider LocationProvider { get; set; }
    }
}