﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="State.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Common.Entities
{
    /// <summary>
    ///     The state of the GPS data provider.
    /// </summary>
    public enum State
    {
        /// <summary>
        ///     Enabled state.
        /// </summary>
        Enabled,

        /// <summary>
        ///     Disabled state.
        /// </summary>
        Disabled,

        /// <summary>
        ///     Initializing state.
        /// </summary>
        Initializing,

        /// <summary>
        ///     Ready state.
        /// </summary>
        Ready,

        /// <summary>
        ///     Unknown state.
        /// </summary>
        Unknown
    }
}