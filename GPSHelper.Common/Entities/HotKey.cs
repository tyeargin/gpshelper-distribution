// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="HotKey.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using GPSHelper.Common.Actions;

namespace GPSHelper.Common.Entities
{
    /// <summary>
    ///     Maps HotKey configuration element to an object.
    /// </summary>
    public class HotKey
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="HotKey" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="action">The action.</param>
        /// <param name="description">The description.</param>
        public HotKey(string value, string action, string description)
        {
            Value = value;
            Action = action;
            Description = description;
        }

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets the action. Action maps to one of the defined Action constants. See <see cref="Actions" />
        /// </summary>
        /// <value>The action.</value>
        public string Action { get; set; }

        /// <summary>
        ///     Gets or sets the value. This is the key or key-combination that should be pressed.
        /// </summary>
        /// <value>The value.</value>
        public string Value { get; set; }
    }
}