﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="InputResult.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using GPSHelper.Common.Actions;

namespace GPSHelper.Common.Entities
{
    /// <summary>
    ///     The result returned by input handler.
    /// </summary>
    public class InputResult
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputResult" /> class.
        /// </summary>
        /// <param name="value">The key combination if input is valid hot key.</param>
        /// <param name="action">The action mapped to the hot key.</param>
        /// <param name="block">if set to <c>true</c> block the input. This is <c>true</c> if the input is a hot key.</param>
        public InputResult(string value, string action, bool block)
        {
            Value = value;
            Action = action;
            Block = block;
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this <see cref="InputResult" /> is block.
        /// </summary>
        /// <value><c>true</c> if result input is a hot key; otherwise, <c>false</c>.</value>
        public bool Block { get; set; }

        /// <summary>
        ///     Gets or sets the action. See <see cref="Actions" />
        /// </summary>
        /// <value>The action.</value>
        public string Action { get; set; }

        /// <summary>
        ///     Gets or sets the value. The key combination.
        /// </summary>
        /// <value>The value.</value>
        public string Value { get; set; }
    }
}