﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="LocationInfo.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using GPSHelper.Common.Contracts;

namespace GPSHelper.Common.Entities
{
    /// <summary>
    ///     Default implementation of ILocationInfo.
    /// </summary>
    /// <seealso cref="ILocationInfo" />
    public class LocationInfo : ILocationInfo
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LocationInfo" /> class.
        /// </summary>
        /// <param name="latitude">The latitude coordinate.</param>
        /// <param name="longitude">The longitude coordinate.</param>
        public LocationInfo(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        /// <summary>
        ///     Stores latitude coordinate.
        /// </summary>
        /// <value>The latitude coordinate.</value>
        public double Latitude { get; set; }

        /// <summary>
        ///     Stores longitude coordinate.
        /// </summary>
        /// <value>The longitude coordinate.</value>
        public double Longitude { get; set; }
    }
}