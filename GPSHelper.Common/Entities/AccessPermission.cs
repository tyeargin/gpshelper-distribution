﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="AccessPermission.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Common.Entities
{
    /// <summary>
    ///     Enum AccessPermission
    /// </summary>
    public enum AccessPermission
    {
        /// <summary>
        ///     Permission allowed.
        /// </summary>
        Allowed,

        /// <summary>
        ///     Permission denied.
        /// </summary>
        Denied,

        /// <summary>
        ///     Permission unknown.
        /// </summary>
        Unknown
    }
}