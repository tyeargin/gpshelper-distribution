﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-23-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="LocationStatusException.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;

namespace GPSHelper.Common.ErrorHandling
{
    /// <summary>
    ///     Exception thrown when an error occurs while retrieve location provider status
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class LocationStatusException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LocationStatusException" /> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public LocationStatusException(string message) : base(message)
        {
        }
    }
}