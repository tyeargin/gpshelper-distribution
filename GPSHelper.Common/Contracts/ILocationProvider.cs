// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="ILocationProvider.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using GPSHelper.Common.Entities;
using GPSHelper.Common.Events;
using GPSHelper.Common.Logging;
using GPSHelper.Common.Modules;

namespace GPSHelper.Common.Contracts
{
    /// <summary>
    ///     Defines the contract ILocationProvider implementations
    /// </summary>
    public interface ILocationProvider
    {
        /// <summary>
        ///     Stores current location information.
        /// </summary>
        /// <value>The location information.</value>
        LocationInfo LocationInfo { get; set; }

        /// <summary>
        ///     Gets the latitude coordinate.
        /// </summary>
        /// <value>The latitude coordinate.</value>
        double Latitude { get; }

        /// <summary>
        ///     Gets the longitude coordinate.
        /// </summary>
        /// <value>The longitude coordinate.</value>
        double Longitude { get; }

        /// <summary>
        ///     Gets the state of the provider source.
        /// </summary>
        /// <value>The state of the provider source.</value>
        State State { get; }

        /// <summary>
        ///     Gets the access permission indicating whether or not the application has access to retrieve GPS info.
        /// </summary>
        /// <value>The access permission.</value>
        AccessPermission AccessPermission { get; }

        /// <summary>
        ///     Displays the current location using online resources.
        /// </summary>
        /// <value>The handling function</value>
        EventHandler ViewLocationOnline { get; }

        /// <summary>
        ///     Gets or sets the URL to the online resource.
        /// </summary>
        /// <value>The URL.</value>
        string Url { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether lower-precision coordinates are output.
        /// </summary>
        /// <value><c>true</c> if lower-precision coordinates are provider; otherwise, <c>false</c>.</value>
        bool AllowLowerPrecision { get; set; }

        /// <summary>
        ///     Gets or sets the events that are fired when coordinates updated.
        /// </summary>
        /// <value>The delegate.</value>
        GpsHelperDelegate<CoordinatesUpdatedEventArgs> CoordinatesUpdated { get; set; }

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        /// <value>The logger.</value>
        ILogger Logger { get; set; }

        /// <summary>
        ///     Initializes this instance of ILocationProvider implementation.
        /// </summary>
        void Initialize();

        /// <summary>
        ///     Starts this instance as needed.
        /// </summary>
        void Start();

        /// <summary>
        ///     If implementation uses GeoCoordinateWatcher a bool value can be passed to indicate whether or not to
        ///     suppress a prompt for permission to use Windows location information.
        /// </summary>
        /// <param name="doNotAskForPermission">if set to <c>true</c> suppress the prompt.</param>
        void Start(bool doNotAskForPermission);

        /// <summary>
        ///     Stops this instance as needed.
        /// </summary>
        void Stop();

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        void Dispose();
    }
}