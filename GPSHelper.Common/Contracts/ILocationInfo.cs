﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="ILocationInfo.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Common.Contracts
{
    /// <summary>
    ///     Defines contract for ILocationInfo implementations
    /// </summary>
    public interface ILocationInfo
    {
        /// <summary>
        ///     Stores latitude coordinate.
        /// </summary>
        /// <value>The latitude coordinate.</value>
        double Latitude { get; set; }

        /// <summary>
        ///     Stores longitude coordinate.
        /// </summary>
        /// <value>The longitude coordinate.</value>
        double Longitude { get; set; }
    }
}