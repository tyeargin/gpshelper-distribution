﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="IInputProvider.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Collections.Generic;
using System.Windows.Forms;
using GPSHelper.Common.Entities;

namespace GPSHelper.Common.Contracts
{
    /// <summary>
    ///     Defines contract for IInputProvider implementations.
    /// </summary>
    public interface IInputProvider
    {
        /// <summary>
        ///     Gets or sets the hot keys defined by the application.
        /// </summary>
        /// <value>The hot keys.</value>
        List<HotKey> HotKeys { get; set; }

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        void Dispose();

        /// <summary>
        ///     Initializes this instance of IInputProvider implementation.
        /// </summary>
        void Initialize();

        /// <summary>
        ///     Sends the text to destination.
        /// </summary>
        /// <param name="text">The text.</param>
        void SendText(string text);

        /// <summary>
        ///     Sends the key to destination.
        /// </summary>
        /// <param name="key">The key.</param>
        void SendKey(VirtualKeyCode key);

        /// <summary>
        ///     Converts the input to the expected HotKey format.
        /// </summary>
        /// <param name="vkCode">The vk code.</param>
        /// <param name="control">The control.</param>
        /// <param name="shift">The shift.</param>
        /// <param name="alt">The alt.</param>
        /// <returns>System.String.</returns>
        string ConvertInput(Keys vkCode, Keys control, Keys shift, Keys alt);

        /// <summary>
        ///     Determines if the input is a valid HotKey or should be ignored.
        /// </summary>
        /// <param name="vkCode">The vk code.</param>
        /// <param name="modifierKeys">The modifier keys.</param>
        /// <param name="altModifier">if set to <c>true</c> Alt was pressed in combination with the key.</param>
        /// <returns>InputResult.</returns>
        InputResult HandleInput(Keys vkCode, Keys modifierKeys, bool altModifier);

        /// <summary>
        ///     Converts the input to the expected HotKey format.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="control">The Control key.</param>
        /// <param name="shift">The shift key.</param>
        /// <param name="alt">The Alt key.</param>
        /// <returns>System.String.</returns>
        string ConvertInput(string key, Keys control, Keys shift, Keys alt);
    }
}