// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-24-2016
// ***********************************************************************
// <copyright file="GpsHelperDelegate.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Common.Modules
{
    /// <summary>
    ///     Generic delegate that is used to extend application functionality
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="e">The e.</param>
    public delegate void GpsHelperDelegate<T>(T e);
}