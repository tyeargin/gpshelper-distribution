// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="IGpsHelperModule.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Collections.Specialized;
using GPSHelper.Common.Events;

namespace GPSHelper.Common.Modules
{
    /// <summary>
    ///     Defines contract for IGpsHelperModule implementations
    /// </summary>
    public interface IGpsHelperModule
    {
        /// <summary>
        ///     Initializes an instance of the implementation.
        /// </summary>
        /// <param name="events">The applications events that the module can subscribe to.</param>
        /// <param name="parameters">The configuration parameters for the module.</param>
        void Initialize(GpsHelperEvents events, NameValueCollection parameters);
    }
}