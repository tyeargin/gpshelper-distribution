﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-18-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="Pattern.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Linq;
using System.Windows.Forms;

namespace GPSHelper.Common.RegularExpressions
{
    /// <summary>
    ///     Regular expression patterns used in the application.
    /// </summary>
    public class Pattern
    {
        /// <summary>
        ///     Used the check if error is related to a missing configuration attribute.
        /// </summary>
        public const string MissingConfigurationAttribute =
            @"Required attribute (['a-zA-Z0-9]*) not found. \([A-Z-a-z0-9.\\: ]*.config{1} line [0-9]*\)";

        /// <summary>
        ///     Used the check if error is related to an unrecognized configuration attribute.
        /// </summary>
        public const string UnrecognizedConfigurationAttribute =
            @"Unrecognized attribute (['a-zA-Z0-9]*). Note that attribute names are case-sensitive. \([A-Z-a-z0-9.\\: ]*.config{1} line [0-9]*\)";

        /// <summary>
        ///     Used the check if error is related to a null type argument.
        /// </summary>
        public const string TypeArgumentNull = "Value cannot be null.\\r\\nParameter name: ([a-zA-Z0-9_]*)";

        /// <summary>
        ///     Used the check if string value matches hot key format.
        /// </summary>
        /// <value>The hot key pattern.</value>
        public static string HotKeyPattern
        {
            get
            {
                Func<string> patternConstructor = () =>
                {
                    var specialKeys = new[]
                    {
                        Keys.Control.ToString(), Keys.Alt.ToString(), Keys.Shift.ToString(), Keys.Up.ToString(),
                        Keys.Down.ToString(), Keys.Left.ToString(), Keys.Right.ToString(), Keys.PageUp.ToString(),
                        Keys.PageDown.ToString(), Keys.Next.ToString()
                    };

                    var specialPart = "";

                    foreach (var s in specialKeys)
                    {
                        specialPart += @"([\+ ]{0,}(" + s.ToUpper() + @")[\+ ]{0,})";

                        if (s != specialKeys.Last())
                        {
                            specialPart += "|";
                        }
                    }

                    var pattern =
                        @"(" + specialPart + @")*([A-Z,.\/-=\[ \]\\`])*" + @"(" + specialPart + @")*";

                    return pattern;
                };

                return patternConstructor();
            }
        }
    }
}