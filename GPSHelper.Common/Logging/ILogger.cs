// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-24-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="ILogger.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Common.Logging
{
    /// <summary>
    ///     Interface ILogger
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        ///     Log message as information.
        /// </summary>
        /// <param name="message">The message.</param>
        void Info(string message);

        /// <summary>
        ///     Log message as error.
        /// </summary>
        /// <param name="message">The message.</param>
        void Error(string message);

        /// <summary>
        ///     Log message as warning.
        /// </summary>
        /// <param name="message">The message.</param>
        void Warning(string message);
    }
}