﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-21-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-21-2016
// ***********************************************************************
// <copyright file="LoggingException.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;

namespace GPSHelper.Common.Logging
{
    /// <summary>
    ///     Exception that is throw if logging error occurs.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class LoggingException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LoggingException" /> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public LoggingException(string message) : base(message)
        {
        }
    }
}