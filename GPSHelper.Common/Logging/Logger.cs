﻿// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-21-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="Logger.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using GPSHelper.Common.Modules;
using log4net;

namespace GPSHelper.Common.Logging
{
    /// <summary>
    ///     Logger implementation.
    /// </summary>
    /// <seealso cref="ILogger" />
    public class Logger : ILogger
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Logger" /> class.
        /// </summary>
        /// <param name="implementation">The implementation.</param>
        public Logger(LoggerImplementation implementation)
        {
            Implementation = implementation;
        }

        /// <summary>
        ///     Gets or sets the implementation.
        /// </summary>
        /// <value>The implementation.</value>
        public LoggerImplementation Implementation { get; set; }

        /// <summary>
        ///     Log message as information.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <exception cref="GPSHelper.Common.Logging.LoggingException"></exception>
        public void Info(string message)
        {
            try
            {
                if (Implementation == LoggerImplementation.Log4Net)
                {
                    var logger = LogManager.GetLogger(typeof (Logger));
                    logger.Info(message);
                }
            }
            catch (Exception e)
            {
                throw new LoggingException(e.Message + " Implementation:" + Implementation);
            }
        }

        /// <summary>
        ///     Log message as error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <exception cref="GPSHelper.Common.Logging.LoggingException"></exception>
        public void Error(string message)
        {
            try
            {
                if (Implementation == LoggerImplementation.Log4Net)
                {
                    var logger = LogManager.GetLogger(typeof (Logger));
                    logger.Error(message);
                }
            }
            catch (Exception e)
            {
                throw new LoggingException(e.Message + " Implementation:" + Implementation);
            }
        }

        /// <summary>
        ///     Log message as warning.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <exception cref="GPSHelper.Common.Logging.LoggingException"></exception>
        public void Warning(string message)
        {
            try
            {
                if (Implementation == LoggerImplementation.Log4Net)
                {
                    var logger = LogManager.GetLogger(typeof (Logger));
                    logger.Warn(message);
                }
            }
            catch (Exception e)
            {
                throw new LoggingException(e.Message + " Implementation:" + Implementation);
            }
        }
    }
}