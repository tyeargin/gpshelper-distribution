// ***********************************************************************
// Assembly         : GPSHelper.Common
// Author           : Travis Yeargin
// Created          : 09-21-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-23-2016
// ***********************************************************************
// <copyright file="LoggerImplementation.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Common.Logging
{
    /// <summary>
    ///     Enumeration of supported third-party or internal logger implementations.
    /// </summary>
    public enum LoggerImplementation
    {
        Log4Net
    }
}