﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="Program.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Threading;
using System.Windows.Forms;
using GPSHelper.Properties;

namespace GPSHelper
{
    /// <summary>
    ///     Class Program.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        ///     The mutex
        /// </summary>
        private static readonly Mutex Mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");

        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            if (Mutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainContainer());
                Mutex.ReleaseMutex();
            }
            else
            {
                MessageBox.Show(Resources.Already_Running);
            }
        }
    }
}