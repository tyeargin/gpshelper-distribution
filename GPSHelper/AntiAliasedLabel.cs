﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-22-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-22-2016
// ***********************************************************************
// <copyright file="AntiAliasedLabel.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Drawing.Text;
using System.Windows.Forms;

namespace GPSHelper
{
    /// <summary>
    ///     A custom implementation of Label control that allows Antialiasing to be enabled.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Label" />
    public partial class AntiAliasedLabel : Label
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AntiAliasedLabel" /> control.
        /// </summary>
        public AntiAliasedLabel()
        {
            InitializeComponent();
        }

        #region Properties

        /// <summary>
        ///     Gets or sets the text rendering hint. Specifies how to render the text.
        /// </summary>
        /// <value>The text rendering hint.</value>
        public TextRenderingHint TextRenderingHint { get; set; } = TextRenderingHint.AntiAlias;

        #endregion

        #region Members

        /// <summary>
        ///     Handles the <see cref="E:Paint" /> event. Overrides the default behaviour to apply the text rendering.
        /// </summary>
        /// <param name="pe">The <see cref="PaintEventArgs" /> instance containing the event data.</param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            pe.Graphics.TextRenderingHint = TextRenderingHint;
            base.OnPaint(pe);
        }

        #endregion
    }
}