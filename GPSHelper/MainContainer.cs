﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="MainContainer.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using GPSHelper.Common.Actions;
using GPSHelper.Common.Entities;
using GPSHelper.Common.ErrorHandling;
using GPSHelper.Common.Events;
using GPSHelper.Common.Logging;
using GPSHelper.Common.Modules;
using GPSHelper.Core;
using GPSHelper.Core.Configuration;
using GPSHelper.Core.Contracts;
using GPSHelper.Input;
using GPSHelper.Notifier;
using GPSHelper.Properties;

namespace GPSHelper
{
    /// <summary>
    ///     The main container (form) of the application.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    /// <seealso cref="IKeyboardListener" />
    /// <remarks>The container is never visible and only acts as a container for the notification area icon.</remarks>
    public partial class MainContainer : Form, IKeyboardListener
    {
        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_TRANSPARENT = 0x20;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainContainer" /> class. Initializes form components.
        /// </summary>
        public MainContainer()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Gets the desktop window.
        /// </summary>
        /// <returns>IntPtr.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetDesktopWindow();

        /// <summary>
        ///     Finds the window.
        /// </summary>
        /// <param name="lpClassName">Window class name.</param>
        /// <param name="lpWindowName">Window name</param>
        /// <returns>IntPtr.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(
            [MarshalAs(UnmanagedType.LPTStr)] string lpClassName,
            [MarshalAs(UnmanagedType.LPTStr)] string lpWindowName);

        /// <summary>
        ///     Sets the parent of the window.
        /// </summary>
        /// <param name="hWndChild">The child window handle.</param>
        /// <param name="hWndNewParent">The parent window handle.</param>
        /// <returns>IntPtr.</returns>
        [DllImport("user32.dll")]
        public static extern IntPtr SetParent(
            IntPtr hWndChild, // handle to window
            IntPtr hWndNewParent // new parent window
            );

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private void MainContainer_Shown(object sender, EventArgs e)
        {
            var exstyle = GetWindowLong(Handle, GWL_EXSTYLE);
            exstyle |= WS_EX_TRANSPARENT;
            SetWindowLong(Handle, GWL_EXSTYLE, exstyle);
            var hwndf = Handle;
            var hwndParent = GetDesktopWindow();
            SetParent(hwndf, hwndParent);
            TopMost = true;
        }

        #region Properties

        /// <summary>
        ///     Gets or sets the application logger.
        /// </summary>
        /// <value>The logger.</value>
        public ILogger Logger { get; set; }

        /// <summary>
        ///     Gets or sets the application events. Contains delegated events that can be subscribe to.
        /// </summary>
        /// <value>The application events.</value>
        public GpsHelperEvents ApplicationEvents { get; set; }

        /// <summary>
        ///     Input Service provides input output functionality such as sending text to Windows and detecting hot key presses.
        /// </summary>
        /// <value>The input service.</value>
        public IInputService InputService { get; set; }

        /// <summary>
        ///     Location manager provides functionality for retrieving location information from Windows.
        /// </summary>
        /// <value>The location manager.</value>
        public ILocationManager LocationManager { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether to allow lower precision when retrieving coordinates.
        /// </summary>
        /// <value><c>true</c> if lower precision is allowed otherwise, <c>false</c>.</value>
        public bool EnableAbbreviatedCoordinates
        {
            get { return LocationManager.EnableAbbreviatedCoordinates; }
            set
            {
                LocationManager.EnableAbbreviatedCoordinates = value;
                LocationManager.UpdateCoordinates();
            }
        }

        /// <summary>
        ///     Gets an instance of MainContainer as IKeyboardListener implementation. This allows <c>KeyboardInterceptor</c> to be
        ///     wired up.
        /// </summary>
        /// <value>The instance.</value>
        public static IKeyboardListener Instance { get; set; }

        /// <summary>
        ///     Gets or sets the notifier.
        /// </summary>
        /// <value>The notifier.</value>
        private Notifier.Notifier NotifierIcon { get; set; }

        public bool Initialized { get; private set; } = false;

        /// <summary>
        ///     Provides mechanism for sending keyboard input to Windows, registering hotkeys, and detecting hotkey presses.
        /// </summary>
        /// <value>The keyboard simulator.</value>
        public static IInputService Keyboard { get; set; }

        /// <summary>
        ///     Provides mechanism for hooking into Windows keyboard input chain. Registers this instance of the application as a
        ///     "listener."
        /// </summary>
        /// <value>The keyboard interceptor.</value>
        /// <remarks>
        ///     The interceptor will block key input if "special key" is pressed. This allow "hot keys" to be used correctly.
        ///     As such you should not use critical keys such as DELETE or ENTER as a hot key since the input will be blocked
        ///     whenever the assigned key/key combination is pressed.
        /// </remarks>
        public static KeyboardInterceptor KeyboardInterceptor { get; set; }

        #endregion Properties

        #region Application

        /// <summary>
        ///     Initializes this instance. Configures components that are required at load time.
        /// </summary>
        private void Initialize()
        {
            Instance = this;

            try
            {
                IConfigurationFactory factory = new ConfigurationFactory();

                //at this point the core components have not been allowed to initialize.
                //load and initialize any extension modules first so that they can subscribe
                //to events that allow resources to be shared with the modules
                factory.LoadModules();

                //configure logging
                Logger = new Logger(LoggerImplementation.Log4Net);

                ApplicationEvents = factory.GpsHelperEvents;

                //share logger
                ApplicationEvents.LoggerAvailable?.Invoke(new ShareLoggerEventArgs(Logger));

                NotifierIcon = new Notifier.Notifier(this);
                NotifierIcon.MenuBuilder = new GpsHelperContextMenuBuilder(NotifierIcon);

                InputService = new InputService(factory);
                LocationManager = new LocationManager(factory);

                Keyboard = InputService;
                KeyboardInterceptor = new KeyboardInterceptor(this);

                RegisterHotKeys();

                Initialized = true;
            }
            catch (LocationStatusException lse)
            {
                MessageBox.Show(lse.Message);

                Initialized = false;
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(Resources.ApplicationInitError, e.Message));

                Initialized = false;

                Application.Exit();
            }
        }

        /// <summary>
        ///     Handles the Load event of the MainContainer control. Fires when form is loaded.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void MainContainer_Load(object sender, EventArgs e)
        {
            try
            {
                //Don't show the form or taskbar icon
                ShowInTaskbar = false;
                Visible = false;

                Application.ApplicationExit += OnApplicationExit;

                Initialize();

                if (Initialized)
                {
                    NotifierIcon.SetNotifierText("GPSHelper - Started");
                    GPSHelper.Notifier.Notifier.ShowInfoNotification(Resources.GPS_Helper_StartedTitle,
                        Resources.GPS_Helper_StartedText, 3000);

                    ApplicationEvents.FormLoaded?.Invoke(new FormLoadedEventArgs(this, gpsTitle, latitudeLabel,
                        longitudeLabel, latitudeText, longitudeText));

                    Thread.Sleep(3000);
                    LocationManager.UpdateCoordinates();
                }
            }
            catch (LocationStatusException lse)
            {
                MessageBox.Show(lse.Message);
            }
            catch (Exception exception)
            {
                MessageBox.Show(string.Format(Resources.ApplicationLoadError,
                    exception.Message));

                Application.Exit();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the window will be activated when it is shown.
        /// </summary>
        /// <value><c>true</c> if [show without activation]; otherwise, <c>false</c>.</value>
        protected override bool ShowWithoutActivation => true;

        /// <summary>
        ///     Handles the <see cref="E:ApplicationExit" /> event. Fires when application is closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnApplicationExit(object sender, EventArgs e)
        {
            try
            {
                LocationManager?.Dispose();
                InputService?.Dispose();

                if (NotifierIcon != null)
                {
                    NotifierIcon.Visible = false;
                    NotifierIcon.EnabledIcon = null; // required to make icon disappear
                    NotifierIcon.Dispose();
                    NotifierIcon = null;

                    KeyboardInterceptor?.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(Resources.ApplicationExitError, ex.Message));
            }
        }

        #endregion Application

        #region Input Handling

        /// <summary>
        ///     Handles the keyboard input.
        /// </summary>
        /// <param name="vkCode">The virtual key code that was detected.</param>
        /// <param name="modifierKeys">
        ///     The modifier keys that were pressed. These include CTRL and SHIFT. ALT is handled separately
        ///     because it is a system key.
        /// </param>
        /// <param name="altModifier">if set to <c>true</c>ALT modifier is present.</param>
        /// <returns><c>true</c> if key or key combination is an application "special key", <c>false</c> otherwise.</returns>
        public bool HandleInput(Keys vkCode, Keys modifierKeys, bool altModifier)
        {
            try
            {
                var result = InputService.HandleInput(vkCode, modifierKeys, altModifier);

                if (LocationManager.Permission == AccessPermission.Allowed)
                    ExecuteHotkeyAction(result.Action);
                else
                {
                    if(!result.Block)
                        Notifier.Notifier.ShowWarningNotification("Location Info Unavailable", "Please verify that Windows Location platform is enabled.", 1500);
                }

                return result.Block;
            }
            catch (LocationStatusException lse)
            {
                MessageBox.Show(lse.Message);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.InputHandlingError + e.Message);
                return true;
            }
        }

        /// <summary>
        ///     If a "special key" is passed as input then executes the action associated with the "special key".
        /// </summary>
        /// <param name="input">The key input.</param>
        /// <exception cref="System.ApplicationException"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void ExecuteHotkeyAction(string input)
        {
            try
            {
                if (input != null)
                {
                    InputService.ActionDictionary[input]?.DynamicInvoke();
                }
            }
            catch (LocationStatusException lse)
            {
                MessageBox.Show(lse.Message);
            }
            catch (Exception e)
            {
                throw new ApplicationException(
                    string.Format(Resources.HotKeyExecutionError, input,
                        e.Message));
            }
        }

        /// <summary>
        ///     Registers the special keys that have associated ActionDictionary that execute when the special key or key
        ///     combination is
        ///     detected.
        /// </summary>
        public void RegisterHotKeys()
        {
            try
            {
                InputService.RegisterHotKey(Actions.OutputLatitude, () =>
                {
                    var xCoord = GetXCoordinate();
                    Keyboard.SendText(xCoord);
                });
                InputService.RegisterHotKey(Actions.OutputLongitude, () =>
                {
                    var yCoord = GetYCoordinate();
                    Keyboard.SendText(yCoord);
                });
                InputService.RegisterHotKey(Actions.OutputCoordinates, () =>
                {
                    var coords = GetCoordinates();
                    Keyboard.SendText(coords.Latitude + ", " + coords.Longitude);
                });

                //allow extensions to register new hot keys
                var registerHotKeyActionsEventArgs = new RegisterHotKeyActionEventArgs(InputService.ActionDictionary);
                ApplicationEvents.HotKeyActionsRegistrationAvailable?.Invoke(registerHotKeyActionsEventArgs);

            }
            catch (LocationStatusException lse)
            {
                MessageBox.Show(lse.Message);
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(Resources.HotKeyRegistrationError, e.Message));
            }
        }

        /// <summary>
        ///     Removes the keyboard interceptor from this instance of the application.
        /// </summary>
        public void RemoveKeyboardInterceptor()
        {
            KeyboardInterceptor?.Dispose();
            KeyboardInterceptor = null;
        }

        /// <summary>
        ///     Adds the keyboard interceptor to this instance of the application.
        /// </summary>
        public static void AddKeyboardInterceptor()
        {
            if (KeyboardInterceptor == null)
                KeyboardInterceptor = new KeyboardInterceptor(Instance);
        }

        /// <summary>
        ///     Toggles the hot keys on and off.
        /// </summary>
        public void ToggleHotKeys()
        {
            InputService.ToggleHotKeys();

            if (InputService.HotKeysEnabled)
            {
                AddKeyboardInterceptor();
            }
            else
            {
                RemoveKeyboardInterceptor();
            }
        }

        #endregion Input Handling

        #region Location

        /// <summary>
        ///     Stops the location updates. When location updates are stopped location information will retain the value of the
        ///     last updated coordinates.
        /// </summary>
        public void StopLocationUpdates()
        {
            LocationManager.Stop();
        }

        /// <summary>
        ///     Starts the location updates. When location updates are enabled the application will watch for location sensor
        ///     updates.
        /// </summary>
        /// <param name="askForPermission">
        ///     if set to <c>true</c> user will be prompted to allow the program to use location
        ///     platform.
        /// </param>
        public void StartLocationUpdates(bool askForPermission)
        {
            LocationManager.Start(askForPermission);
        }

        /// <summary>
        ///     Gets the GPS latitude coordinate
        /// </summary>
        /// <returns>System.String.</returns>
        private string GetXCoordinate()
        {
            return LocationManager.Latitude.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Gets the GPS longitude coordinate.
        /// </summary>
        /// <returns>System.String.</returns>
        private string GetYCoordinate()
        {
            return LocationManager.Longitude.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Gets the current GPS coordinates.
        /// </summary>
        /// <returns>System.String.</returns>
        private LocationInfo GetCoordinates()
        {
            return LocationManager.GetLocationInfo();
        }

        /// <summary>
        ///     Views the location online. Opens provider site in default browser.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        public void ViewLocationOnline(object sender, EventArgs e)
        {
            LocationManager.ViewLocationOnline(sender, e);
        }

        #endregion
    }
}