﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="INotifier.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Drawing;
using System.Windows.Forms;

namespace GPSHelper.Notifier
{
    /// <summary>
    ///     Interface INotifier
    /// </summary>
    public interface INotifier
    {
        /// <summary>
        ///     Gets or sets the icon disabled path.
        /// </summary>
        /// <value>The icon disabled path.</value>
        string IconDisabledPath { get; set; }

        /// <summary>
        ///     Gets or sets the active icon.
        /// </summary>
        /// <value>The active icon.</value>
        Icon ActiveIcon { get; set; }

        /// <summary>
        ///     Gets or sets the disabled icon.
        /// </summary>
        /// <value>The disabled icon.</value>
        Icon DisabledIcon { get; set; }

        /// <summary>
        ///     Gets or sets the enabled icon.
        /// </summary>
        /// <value>The enabled icon.</value>
        Icon EnabledIcon { get; set; }

        /// <summary>
        ///     Gets or sets the menu.
        /// </summary>
        /// <value>The menu.</value>
        ContextMenu Menu { get; set; }

        /// <summary>
        ///     Gets or sets the menu builder.
        /// </summary>
        /// <value>The menu builder.</value>
        IBuildNotifierMenu MenuBuilder { get; set; }

        /// <summary>
        ///     Gets or sets the container.
        /// </summary>
        /// <value>The container.</value>
        MainContainer Container { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this <see cref="INotifier" /> is visible.
        /// </summary>
        /// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
        bool Visible { get; set; }

        /// <summary>
        ///     Sets the activator.
        /// </summary>
        /// <param name="action">The action.</param>
        void SetActivator(EventHandler action);

        /// <summary>
        ///     Disposes this instance.
        /// </summary>
        void Dispose();

        /// <summary>
        ///     Sets the notifier text.
        /// </summary>
        /// <param name="text">The text.</param>
        void SetNotifierText(string text);
    }
}