﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="GpsHelperContextMenuBuilder.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Windows.Forms;
using GPSHelper.Properties;

namespace GPSHelper.Notifier
{
    /// <summary>
    ///     Responsible for building Notifier context-menu.
    /// </summary>
    /// <seealso cref="IBuildNotifierMenu" />
    public class GpsHelperContextMenuBuilder : IBuildNotifierMenu
    {
        /// <summary>
        ///     The _notifier
        /// </summary>
        private readonly INotifier _notifier;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GpsHelperContextMenuBuilder" /> class.
        /// </summary>
        /// <param name="notifier">The notifier.</param>
        internal GpsHelperContextMenuBuilder(INotifier notifier)
        {
            _notifier = notifier;

            var handler = new EventHandler((sender, args) => { EnableLocationUpdatesMenuItem.PerformClick(); });

            _notifier.SetActivator(handler);

            Build();
        }

        #region Members

        /// <summary>
        ///     Builds the context-menu.
        /// </summary>
        public void Build()
        {
            try
            {
                Menu = _notifier.Menu;

                EnableLocationUpdatesMenuItem = new MenuItem {Text = Resources.Menu_StopUpdatesItem};
                EnableHotkeyMenuItem = new MenuItem {Text = Resources.Menu_DisableHotkeysItem};
                EnableAbbreviatedCoordinatesMenuItem = new MenuItem {Text = Resources.Menu_DisableShortCoordinatesItem};
                ShowLocationMenuItem = new MenuItem {Text = Resources.Menu_ShowOnMapItem};
                QuitMenuItem = new MenuItem {Text = Resources.Menu_ExitItem};

                RegisterClickEvent(EnableLocationUpdatesMenuItem, OnClickEnableUpdates);
                RegisterClickEvent(QuitMenuItem, OnClickQuit);
                RegisterClickEvent(EnableHotkeyMenuItem, OnClickEnableHotkeys);
                RegisterClickEvent(EnableAbbreviatedCoordinatesMenuItem, OnClickEnableShortCoordinates);
                RegisterClickEvent(ShowLocationMenuItem, _notifier.Container.ViewLocationOnline);

                EnableLocationUpdatesMenuItem.Visible = true;
                EnableLocationUpdatesMenuItem.Checked = true;
                EnableHotkeyMenuItem.Checked = true;
                EnableAbbreviatedCoordinatesMenuItem.Checked = true;

                Menu.MenuItems.Add(EnableLocationUpdatesMenuItem);
                Menu.MenuItems.Add(EnableHotkeyMenuItem);
                Menu.MenuItems.Add(EnableAbbreviatedCoordinatesMenuItem);
                Menu.MenuItems.Add(ShowLocationMenuItem);
                Menu.MenuItems.Add(QuitMenuItem);

                _notifier.Menu = Menu;
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(Resources.NotifierMenuInitError, e.Message));
            }
        }

        /// <summary>
        ///     Handles the <see cref="E:ClickEnableHotkeys" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnClickEnableHotkeys(object sender, EventArgs e)
        {
            EnableHotkeyMenuItem.Checked = !EnableHotkeyMenuItem.Checked;
            _notifier.Container.ToggleHotKeys();

            EnableHotkeyMenuItem.Text = EnableHotkeyMenuItem.Checked
                ? "Enable Hotkeys"
                : "Enable Hotkeys";

            if (EnableHotkeyMenuItem.Checked == false)
            {
                Notifier.ShowInfoNotification("GPSHelper Hotkeys Disabled",
                    "GPSHelper Hotkeys have been disabled.", 3000);
            }
            else
            {
                Notifier.ShowInfoNotification("GPSHelper Hotkeys Enabled",
                    "GPSHelper Hotkeys have been enabled.", 3000);
            }
        }

        /// <summary>
        ///     Handles the <see cref="E:ClickEnableShortCoordinates" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnClickEnableShortCoordinates(object sender, EventArgs e)
        {
            EnableAbbreviatedCoordinatesMenuItem.Checked = !EnableAbbreviatedCoordinatesMenuItem.Checked;

            EnableAbbreviatedCoordinatesMenuItem.Text = EnableAbbreviatedCoordinatesMenuItem.Checked
                ? "Enable Lower-Precision Coordinates"
                : "Enable Lower-Precision Coordinates";

            if (EnableAbbreviatedCoordinatesMenuItem.Checked == false)
            {
                _notifier.Container.EnableAbbreviatedCoordinates = false;
                Notifier.ShowInfoNotification("Lower-Precision Coordinates Disabled",
                    "Lower-Precision coordinates have been disabled.", 3000);
            }
            else
            {
                _notifier.Container.EnableAbbreviatedCoordinates = true;
                Notifier.ShowInfoNotification("Lower-Precision Coordinates Enabled",
                    "Lower-Precision coordinates have been enabled.", 3000);
            }
        }

        /// <summary>
        ///     Handles the <see cref="E:ClickQuit" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnClickQuit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        ///     Handles the <see cref="E:ClickEnableUpdates" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnClickEnableUpdates(object sender, EventArgs e)
        {
            EnableLocationUpdatesMenuItem.Checked = !EnableLocationUpdatesMenuItem.Checked;

            EnableLocationUpdatesMenuItem.Text = !EnableLocationUpdatesMenuItem.Checked
                ? Resources.Menu_EnableUpdatesItem
                : Resources.Menu_StopUpdatesItem;

            //if option toggled off
            if (EnableLocationUpdatesMenuItem.Checked == false)
            {
                _notifier.Container.StopLocationUpdates();
                _notifier.SetNotifierText("GPSHelper - Stopped");
                _notifier.ActiveIcon = _notifier.DisabledIcon;

                Notifier.ShowInfoNotification("GPSHelper Coordinate Updates Disabled",
                    "GPSHelper coordinate updates have been disabled.", 3000);
            }
            else
            {
                _notifier.Container.StartLocationUpdates(false);
                _notifier.SetNotifierText("GPSHelper - Started");
                _notifier.ActiveIcon = _notifier.EnabledIcon;

                Notifier.ShowInfoNotification("GPSHelper Coordinate Updates Enabled",
                    "GPSHelper coordinate updates have been enabled.", 3000);
            }
        }

        /// <summary>
        ///     Registers the click event.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="callback">The callback.</param>
        private void RegisterClickEvent(MenuItem target, EventHandler callback)
        {
            target.Click += callback;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the menu.
        /// </summary>
        /// <value>The menu.</value>
        private ContextMenu Menu { get; set; }

        /// <summary>
        ///     Gets or sets the enable location updates menu item. Toggles location updates.
        /// </summary>
        /// <value>The enable location updates menu item.</value>
        public MenuItem EnableLocationUpdatesMenuItem { get; set; }

        /// <summary>
        ///     Gets or sets the quit menu item. Exits the application.
        /// </summary>
        /// <value>The quit menu item.</value>
        public MenuItem QuitMenuItem { get; set; }

        /// <summary>
        ///     Gets or sets the enable hotkey menu item. Toggles hotkeys.
        /// </summary>
        /// <value>The enable hotkey menu item.</value>
        public MenuItem EnableHotkeyMenuItem { get; set; }

        /// <summary>
        ///     Gets or sets the enable abbreviated coordinates menu item. Enables or disable allowance of lower precision
        ///     coordinates output.
        /// </summary>
        /// <value>The enable abbreviated coordinates menu item.</value>
        public static MenuItem EnableAbbreviatedCoordinatesMenuItem { get; set; }

        /// <summary>
        ///     Gets or sets the show location menu item. Display current location online.
        /// </summary>
        /// <value>The show location menu item.</value>
        public MenuItem ShowLocationMenuItem { get; set; }

        #endregion Properties
    }
}