﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="IBuildNotifierMenu.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace GPSHelper.Notifier
{
    /// <summary>
    ///     Interface IBuildNotifierMenu
    /// </summary>
    public interface IBuildNotifierMenu
    {
        /// <summary>
        ///     Builds context menu.
        /// </summary>
        void Build();
    }
}