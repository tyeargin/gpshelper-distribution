﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="Notifier.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using GPSHelper.Properties;

namespace GPSHelper.Notifier
{
    /// <summary>
    ///     A wrapper class for <c>NotifyIcon</c>.
    /// </summary>
    /// <seealso cref="INotifier" />
    public class Notifier : INotifier
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Notifier" /> class. Creates an instance of NotifyIcon and builds menu.
        /// </summary>
        /// <param name="container">The container.</param>
        public Notifier(MainContainer container)
        {
            Instance = new NotifyIcon();
            RegisterNotificationIcon();
            Menu = new ContextMenu();
            Container = container;
        }

        #region Members

        /// <summary>
        ///     Sets the notifier text. Sets text of the NotifyIcon.
        /// </summary>
        /// <param name="text">The text.</param>
        public void SetNotifierText(string text)
        {
            Instance.Text = text;
        }

        /// <summary>
        ///     Sets the activator action that occurs when the NotifyIcon is double-clicked.
        /// </summary>
        /// <param name="action">The action to perform.</param>
        public void SetActivator(EventHandler action)
        {
            Instance.DoubleClick += action;
        }

        /// <summary>
        ///     Disposes this instance. Frees resources associated with the NotifyIcon.
        /// </summary>
        public void Dispose()
        {
            Instance.Dispose();
        }

        /// <summary>
        ///     Shows the information notification.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="timeout">The timeout.</param>
        internal static void ShowInfoNotification(string title, string text, int timeout)
        {
            Instance.BalloonTipIcon = ToolTipIcon.Info;
            Instance.BalloonTipTitle = title;
            Instance.BalloonTipText = text;
            Instance.ShowBalloonTip(timeout);
        }

        /// <summary>
        ///     Shows the warning notification.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        /// <param name="timeout">The timeout.</param>
        internal static void ShowWarningNotification(string title, string text, int timeout)
        {
            Instance.BalloonTipIcon = ToolTipIcon.Warning;
            Instance.BalloonTipTitle = title;
            Instance.BalloonTipText = text;
            Instance.ShowBalloonTip(timeout);
        }

        /// <summary>
        ///     Registers the notification icon.
        /// </summary>
        private void RegisterNotificationIcon()
        {
            try
            {
                var iconPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (iconPath != null)
                    IconPath = Path.Combine(iconPath,
                        ConfigurationManager.AppSettings.Get("Icon"));

                var iconDisabledPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                if (iconDisabledPath != null)
                    IconDisabledPath = Path.Combine(iconDisabledPath,
                        ConfigurationManager.AppSettings.Get("Icon_Disabled"));
                EnabledIcon = new Icon(IconPath);
                DisabledIcon = new Icon(IconDisabledPath);


                Instance.Icon = EnabledIcon;
                Instance.Visible = true;
                Instance.ContextMenu = Menu;

                SetNotifierText("GPSHelper - Started");
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(Resources.NotificationIconInitError, e.Message));
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets file path of the icon to display when the NotifyIcon is in "disabled" state.
        /// </summary>
        /// <value>The icon disabled path.</value>
        public string IconDisabledPath { get; set; }

        /// <summary>
        ///     Gets or sets file path of the icon to display when the NotifyIcon is in "enabled" state.
        /// </summary>
        /// <value>The icon path.</value>
        public static string IconPath { get; set; }

        /// <summary>
        ///     Gets or sets the current NotifyIcon icon.
        /// </summary>
        /// <value>The active icon.</value>
        public Icon ActiveIcon
        {
            get { return Instance.Icon; }
            set { Instance.Icon = value; }
        }

        /// <summary>
        ///     Gets or sets file path of the icon to display when the NotifyIcon is in "disabled" state.
        /// </summary>
        /// <value>The disabled icon.</value>
        public Icon DisabledIcon { get; set; }

        /// <summary>
        ///     Gets or sets the icon to display when the NotifyIcon is in "enabled" state.
        /// </summary>
        /// <value>The enabled icon.</value>
        public Icon EnabledIcon { get; set; }

        /// <summary>
        ///     Gets or sets the instance.
        /// </summary>
        /// <value>The instance.</value>
        private static NotifyIcon Instance { get; set; }

        /// <summary>
        ///     Gets or sets the NotifyIcon context menu.
        /// </summary>
        /// <value>The menu.</value>
        public ContextMenu Menu
        {
            get { return Instance.ContextMenu; }
            set { Instance.ContextMenu = value; }
        }

        /// <summary>
        ///     Gets or sets the text of the NotifyIcon.
        /// </summary>
        /// <value>The text.</value>
        public static string Text
        {
            get { return Instance.Text; }
            set { Instance.Text = value; }
        }

        /// <summary>
        ///     Responsible for building the NotifyIcon context menu.
        /// </summary>
        /// <value>The menu builder.</value>
        public IBuildNotifierMenu MenuBuilder { get; set; }

        /// <summary>
        ///     Gets or sets the container. The container is the parent form of the NotifyIcon.
        /// </summary>
        /// <value>The container.</value>
        public MainContainer Container { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this <see cref="Notifier" /> is visible or hidden.
        /// </summary>
        /// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
        public bool Visible
        {
            get { return Instance.Visible; }
            set { Instance.Visible = value; }
        }

        #endregion Properties
    }
}