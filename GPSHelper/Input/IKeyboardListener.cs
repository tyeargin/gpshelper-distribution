// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="IKeyboardListener.cs">
//     Copyright �  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Windows.Forms;

namespace GPSHelper.Input
{
    /// <summary>
    ///     Interface IKeyboardListener
    /// </summary>
    public interface IKeyboardListener
    {
        /// <summary>
        ///     Handles the keyboard input.
        /// </summary>
        /// <param name="vkCode">The key code.</param>
        /// <param name="modifierKeys">The modifier keys that were pressed.</param>
        /// <param name="altModifier">if set to <c>true</c> alt modifier key was pressed.</param>
        /// <returns><c>true</c> if key should be blocked, <c>false</c> otherwise.</returns>
        bool HandleInput(Keys vkCode, Keys modifierKeys, bool altModifier);
    }
}