﻿// ***********************************************************************
// Assembly         : GPSHelper
// Author           : Travis Yeargin
// Created          : 09-10-2016
//
// Last Modified By : Travis Yeargin
// Last Modified On : 09-18-2016
// ***********************************************************************
// <copyright file="KeyboardInterceptor.cs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GPSHelper.Input
{
    /// <summary>
    ///     Provides mechanism for establishing a low-level keyboard hook.
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class KeyboardInterceptor : IDisposable
    {
        /// <summary>
        ///     The w h_ keyboar d_ ll
        /// </summary>
        private const int WH_KEYBOARD_LL = 13;

        /// <summary>
        ///     The w m_ keydown
        /// </summary>
        private const int WM_KEYDOWN = 0x0100;

        /// <summary>
        ///     The proc
        /// </summary>
        private static readonly LowLevelKeyboardProc Proc = HookCallback;

        /// <summary>
        ///     The _hook identifier
        /// </summary>
        private static IntPtr _hookId = IntPtr.Zero;

        /// <summary>
        ///     The _listener
        /// </summary>
        private static IKeyboardListener _listener;

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyboardInterceptor" /> class. Sets keyboard hook.
        /// </summary>
        /// <param name="listener">The listener responds to any keyboard input received.</param>
        public KeyboardInterceptor(IKeyboardListener listener)
        {
            _listener = listener;
            _hookId = SetHook(Proc);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources. Removes
        ///     keyboard hook.
        /// </summary>
        public void Dispose()
        {
            _listener = null;
            UnhookWindowsHookEx(_hookId);
        }

        /// <summary>
        ///     Sets the hook.
        /// </summary>
        /// <param name="proc">The proc.</param>
        /// <returns>IntPtr.</returns>
        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (var curProcess = Process.GetCurrentProcess())

            using (var curModule = curProcess.MainModule)

            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }


        /// <summary>
        ///     Hooks the callback.
        /// </summary>
        /// <param name="nCode">The n code.</param>
        /// <param name="wParam">The w parameter.</param>
        /// <param name="lParam">The l parameter.</param>
        /// <returns>IntPtr.</returns>
        private static IntPtr HookCallback(
            int nCode, IntPtr wParam, IntPtr lParam)
        {
            var keypressAllowed = true;

            if (nCode >= 0 && (wParam == (IntPtr) WM_KEYDOWN || wParam == (IntPtr) 260))
            {
                var vkCode = Marshal.ReadInt32(lParam);
                var altModifier = vkCode != 0 && Keys.Alt == (Control.ModifierKeys & Keys.Alt);

                var key = (Keys) vkCode;

                keypressAllowed = _listener.HandleInput(key, Control.ModifierKeys, altModifier);
            }

            if (keypressAllowed)
                return CallNextHookEx(_hookId, nCode, wParam, lParam);

            return (IntPtr) 1;
        }


        /// <summary>
        ///     Sets the windows hook ex.
        /// </summary>
        /// <param name="idHook">The identifier hook.</param>
        /// <param name="lpfn">The LPFN.</param>
        /// <param name="hMod">The h mod.</param>
        /// <param name="dwThreadId">The dw thread identifier.</param>
        /// <returns>IntPtr.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
            LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);


        /// <summary>
        ///     Unhooks the windows hook ex.
        /// </summary>
        /// <param name="hhk">The HHK.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);


        /// <summary>
        ///     Calls the next hook ex.
        /// </summary>
        /// <param name="hhk">The HHK.</param>
        /// <param name="nCode">The n code.</param>
        /// <param name="wParam">The w parameter.</param>
        /// <param name="lParam">The l parameter.</param>
        /// <returns>IntPtr.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
            IntPtr wParam, IntPtr lParam);


        /// <summary>
        ///     Gets the module handle.
        /// </summary>
        /// <param name="lpModuleName">Name of the lp module.</param>
        /// <returns>IntPtr.</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        /// <summary>
        ///     Delegate LowLevelKeyboardProc
        /// </summary>
        /// <param name="nCode">The n code.</param>
        /// <param name="wParam">The w parameter.</param>
        /// <param name="lParam">The l parameter.</param>
        /// <returns>IntPtr.</returns>
        private delegate IntPtr LowLevelKeyboardProc(
            int nCode, IntPtr wParam, IntPtr lParam);
    }
}