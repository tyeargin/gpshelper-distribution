﻿<?xml version="1.0" encoding="utf-8"?>

<configuration>
  <configSections>
    <section name="log4net" type="System.Configuration.IgnoreSectionHandler" />
    <section name="core" type="GPSHelper.Core.Configuration.GpsHelperConfigurationSection,GPSHelper.Core" />
  </configSections>
  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.2" />
  </startup>
  <log4net>
    <appender name="Console" type="log4net.Appender.ConsoleAppender">
      <layout type="log4net.Layout.PatternLayout">
        <!-- Pattern to output the caller's file name and line number -->
        <conversionPattern value="%level [%thread] (%file:%line) - %message%newline" />
      </layout>
    </appender>

    <appender name="RollingFile" type="log4net.Appender.RollingFileAppender">
      <file value="example.log" />
      <appendToFile value="true" />
      <maximumFileSize value="500KB" />
      <maxSizeRollBackups value="2" />

      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%level %thread %logger - %message%newline" />
      </layout>
    </appender>

    <root>
      <level value="DEBUG" />
      <appender-ref ref="Console" />
      <appender-ref ref="RollingFile" />
    </root>
  </log4net>
  <core>
    <!-- 
      LocationProvider provides the location data to the application. It is a wrapper for the native Windows Location platform.
      
      To set update sensitivity set movementThreshold to an integer value, example: movementThreshold="1" would set the movement threshold to 1 meter. This will cause
      the application to poll for an update every 1 meter of positional change.
      
      By default the application uses a medium accuracy level for requesting coordinates from the Location platform. Th use demand a higher accuracy set  
      useHighPositionalAccuracy to true, example: userHighPositionalAccuracy="true"
    -->
    <inputService defaultProvider="InputProvider">
      <providers>
        <add name="InputProvider" type="GPSHelper.Providers.InputProvider,GPSHelper.Providers" />
      </providers>
      <hotkeys>
        <add Description="Output the latitude of the current position." value="CONTROL+," action="OutputLatitude" />
        <add Description="Output the longitude of the current position." value="CONTROL+." action="OutputLongitude" />
        <add Description="Output the coordinates of the current position." value=".+ALT+CONTROL"
             action="OutputCoordinates" />
        <add Description="Output the coordinates of the current position for Report Beam input." value="`"
             action="OutputReportBeamCoordinates" />
        <add Description="Toggle transparent desktop overlay" value="CONTROL+`" action="ToggleOverlay" />
        <add Description="Show location online" value="SHIFT+`" action="ShowLocationOnline" />
      </hotkeys>
    </inputService>
    <locationManager defaultProvider="LocationProvider">
      <providers>
        <add name="LocationProvider" type="GPSHelper.Providers.LocationProvider,GPSHelper.Providers"
             useHighPositionalAccuracy="true" movementThreshold="1" />
      </providers>
    </locationManager>
    <extensions>
      <add name="ReportBeam" type="ReportBeamExtension.ReportBeam,ReportBeamExtension" />
      <add name="Overlay" type="DesktopOverlayExtension.Overlay,DesktopOverlayExtension" offset_top="100"
           offset_left="85" color="orangered" />
    </extensions>
  </core>
  <appSettings>
    <add key="Icon" value="icon.ico" />
    <add key="Icon_Disabled" value="icon2.ico" />
  </appSettings>
</configuration>