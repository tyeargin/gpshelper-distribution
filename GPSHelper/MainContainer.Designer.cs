﻿using System.Drawing;

namespace GPSHelper
{
    partial class MainContainer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainContainer));
            this.gpsTitle = new AntiAliasedLabel();
            this.longitudeText = new AntiAliasedLabel();
            this.latitudeText = new AntiAliasedLabel();
            this.longitudeLabel = new AntiAliasedLabel();
            this.latitudeLabel = new AntiAliasedLabel();
            this.SuspendLayout();
            // 
            // gpsTitle
            // 
            this.gpsTitle.AutoSize = true;
            this.gpsTitle.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpsTitle.ForeColor = System.Drawing.Color.OrangeRed;
            this.gpsTitle.Location = new System.Drawing.Point(13, 13);
            this.gpsTitle.Name = "gpsTitle";
            this.gpsTitle.Size = new System.Drawing.Size(82, 19);
            this.gpsTitle.TabIndex = 0;
            this.gpsTitle.Text = "GPS Info";
            this.gpsTitle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            // 
            // longitudeText
            // 
            this.longitudeText.AutoSize = true;
            this.longitudeText.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.longitudeText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.longitudeText.Location = new System.Drawing.Point(106, 69);
            this.longitudeText.Name = "longitudeText";
            this.longitudeText.Size = new System.Drawing.Size(86, 19);
            this.longitudeText.TabIndex = 1;
            this.longitudeText.Text = "Unknown";
            this.longitudeText.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            // 
            // latitudeText
            // 
            this.latitudeText.AutoSize = true;
            this.latitudeText.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.latitudeText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.latitudeText.Location = new System.Drawing.Point(106, 41);
            this.latitudeText.Name = "latitudeText";
            this.latitudeText.Size = new System.Drawing.Size(86, 19);
            this.latitudeText.TabIndex = 2;
            this.latitudeText.Text = "Unknown";
            this.latitudeText.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            // 
            // longitudeLabel
            // 
            this.longitudeLabel.AutoSize = true;
            this.longitudeLabel.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.longitudeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.longitudeLabel.Location = new System.Drawing.Point(13, 69);
            this.longitudeLabel.Name = "longitudeLabel";
            this.longitudeLabel.Size = new System.Drawing.Size(98, 19);
            this.longitudeLabel.TabIndex = 3;
            this.longitudeLabel.Text = "Longitude:";
            this.longitudeLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            // 
            // latitudeLabel
            // 
            this.latitudeLabel.AutoSize = true;
            this.latitudeLabel.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.latitudeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.latitudeLabel.Location = new System.Drawing.Point(13, 41);
            this.latitudeLabel.Name = "latitudeLabel";
            this.latitudeLabel.Size = new System.Drawing.Size(84, 19);
            this.latitudeLabel.TabIndex = 4;
            this.latitudeLabel.Text = "Latitude:";
            this.latitudeLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            // 
            // MainContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(257, 103);
            this.Controls.Add(this.latitudeLabel);
            this.Controls.Add(this.longitudeLabel);
            this.Controls.Add(this.latitudeText);
            this.Controls.Add(this.longitudeText);
            this.Controls.Add(this.gpsTitle);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.LightGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainContainer";
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.White;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.MainContainer_Load);
            this.Shown += new System.EventHandler(this.MainContainer_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AntiAliasedLabel gpsTitle;
        private AntiAliasedLabel longitudeText;
        private AntiAliasedLabel latitudeText;
        private AntiAliasedLabel longitudeLabel;
        private AntiAliasedLabel latitudeLabel;
    }
}

