﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPSHelper.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GPSHelper.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An instance of the GPSHelper application is already running..
        /// </summary>
        public static string Already_Running {
            get {
                return ResourceManager.GetString("Already_Running", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An exception occurred while exiting the application: {0}.
        /// </summary>
        public static string ApplicationExitError {
            get {
                return ResourceManager.GetString("ApplicationExitError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An exception occurred while initializing the GPSHelper application: {0}
        ///
        ///Application will exit..
        /// </summary>
        public static string ApplicationInitError {
            get {
                return ResourceManager.GetString("ApplicationInitError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An exception occurred while loading the GPSHelper application: {0}
        ///
        ///Application will exit..
        /// </summary>
        public static string ApplicationLoadError {
            get {
                return ResourceManager.GetString("ApplicationLoadError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An exception occurred while retrieve the setting value for the key {0}: {1}.
        /// </summary>
        public static string ConfigurationReadError {
            get {
                return ResourceManager.GetString("ConfigurationReadError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occured while initializing the location provider: {0}.
        /// </summary>
        public static string GeoWatcherInitError {
            get {
                return ResourceManager.GetString("GeoWatcherInitError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GPSHelper ({0}).
        /// </summary>
        public static string GPS_Helper_ShortText {
            get {
                return ResourceManager.GetString("GPS_Helper_ShortText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GPSHelper application successfully initialized..
        /// </summary>
        public static string GPS_Helper_StartedText {
            get {
                return ResourceManager.GetString("GPS_Helper_StartedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GPSHelper Started.
        /// </summary>
        public static string GPS_Helper_StartedTitle {
            get {
                return ResourceManager.GetString("GPS_Helper_StartedTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GPSHelper ({0}) - Coordinates({1}/{2}).
        /// </summary>
        public static string GPS_Helper_Text {
            get {
                return ResourceManager.GetString("GPS_Helper_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occurred while execution the action for the hotkey {0}: {1}.
        /// </summary>
        public static string HotKeyExecutionError {
            get {
                return ResourceManager.GetString("HotKeyExecutionError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occurred while registering the application hot keys: {0}.
        /// </summary>
        public static string HotKeyRegistrationError {
            get {
                return ResourceManager.GetString("HotKeyRegistrationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fatal Error: Input translation failure! Exception: .
        /// </summary>
        public static string InputHandlingError {
            get {
                return ResourceManager.GetString("InputHandlingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occurred while translating input to a hotkey: .
        /// </summary>
        public static string InputTranslationError {
            get {
                return ResourceManager.GetString("InputTranslationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable Hotkeys.
        /// </summary>
        public static string Menu_DisableHotkeysItem {
            get {
                return ResourceManager.GetString("Menu_DisableHotkeysItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable Lower-Precision Coordinates.
        /// </summary>
        public static string Menu_DisableShortCoordinatesItem {
            get {
                return ResourceManager.GetString("Menu_DisableShortCoordinatesItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use Balloon Notifications.
        /// </summary>
        public static string Menu_EnableBalloonNotificationsItem {
            get {
                return ResourceManager.GetString("Menu_EnableBalloonNotificationsItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable Hotkeys.
        /// </summary>
        public static string Menu_EnableHotkeysItem {
            get {
                return ResourceManager.GetString("Menu_EnableHotkeysItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable Report Beam Formatted Coordinates.
        /// </summary>
        public static string Menu_EnableShortCoordinatesItem {
            get {
                return ResourceManager.GetString("Menu_EnableShortCoordinatesItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use Toast Notifications.
        /// </summary>
        public static string Menu_EnableToastNotificationsItem {
            get {
                return ResourceManager.GetString("Menu_EnableToastNotificationsItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable Location Updates.
        /// </summary>
        public static string Menu_EnableUpdatesItem {
            get {
                return ResourceManager.GetString("Menu_EnableUpdatesItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exit.
        /// </summary>
        public static string Menu_ExitItem {
            get {
                return ResourceManager.GetString("Menu_ExitItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Location In Google Maps.
        /// </summary>
        public static string Menu_ShowOnMapItem {
            get {
                return ResourceManager.GetString("Menu_ShowOnMapItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable Location Updates.
        /// </summary>
        public static string Menu_StopUpdatesItem {
            get {
                return ResourceManager.GetString("Menu_StopUpdatesItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An exception occurred while initializing the notifier: {0}.
        /// </summary>
        public static string NotificationIconInitError {
            get {
                return ResourceManager.GetString("NotificationIconInitError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occured while initializing the notifier menu: {0}.
        /// </summary>
        public static string NotifierMenuInitError {
            get {
                return ResourceManager.GetString("NotifierMenuInitError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An exception occurred while update the notifier status info: {0}.
        /// </summary>
        public static string StatusTextUpdateError {
            get {
                return ResourceManager.GetString("StatusTextUpdateError", resourceCulture);
            }
        }
    }
}
